<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li class="{{ Route::currentRouteNamed('store_list') ? 'active' : '' }}"><a href="{{ route('store_list') }}">Список магазинов</a></li>
        @role('admin|moderator')
        <li><a href="{{ route('store_create') }}">Добавить новый магазин</a></li>
        @if(Route::currentRouteNamed('store'))
        <li><a href="{{ route('store_produce_price', ['storeId' => $store->id]) }}">Цена на товары для магазина</a></li>
        @endif
        @endrole
        @if(Route::currentRouteNamed('store'))
        <li><a href="{{ route('store_stock', ['storeId' => $store->id]) }}">Товары на складе магазина</a></li>
        <li><a href="{{ route('store_charges', ['storeId' => $store->id]) }}">Растраты магазина</a></li>
        @endif
    </ul>
</div>