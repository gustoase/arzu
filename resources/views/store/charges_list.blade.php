@extends('layouts.main')

@section('js')
<script src="/js/store.js"></script>
@stop

@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="panel panel-info">

                <div class="panel-heading">Новые расходы в магазине</div>
                <div class="alert alert-danger hide" id="msg" role="alert"></div>
                <table id="new-charge-store" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td><a href="#" id="charge" data-type="select2" data-source='{!! $charges_type_json !!}' data-name="charge_type_id" data-original-title="Тип растрат">тип растрат</a></td>
                        <td><a href="#" data-type="text" data-name="cost" data-original-title="Сумма">укажите сумму</a></td>
                        <td><a href="#" data-type="text" data-name="description" data-original-title="Описание">описание</a></td>
                        <td style="width: 115px"><button id="save-btn" class="btn btn-primary pull-right">Сохранить</button></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="table-responsive">
                <table id="store-charges-list-table" class="table table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Тип</th>
                        <th>Описание</th>
                        <th>Сумма</th>
                        <th>Дата</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop