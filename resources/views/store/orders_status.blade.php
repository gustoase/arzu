<div class="list-group">
    <a href="{{ route('order_new') }}" class="list-group-item list-group-item-info">
        Новый заказ
    </a>
    <a href="?filter=not_completed" class="list-group-item list-group-item-danger">
        Незавершенные заказы
        <span class="badge">{{ $countNotCompleted }}</span>
    </a>
    <a href="?filter=completed" class="list-group-item list-group-item-danger">
        Завершенные заказы
        <span class="badge">{{ $countCompleted }}</span>
    </a>
</div>