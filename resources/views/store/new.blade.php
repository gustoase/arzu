@extends('layouts.main')

@section('js')
<script src="/js/store.js"></script>
@stop

@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="alert alert-danger hide" id="msg" role="alert"></div>
                <table id="new-store" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td width="40%">Название</td>
                        <td><a href="#" data-type="text" data-name="name" data-original-title="Как называется магазин?">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Описание (опционально)</td>
                        <td><a href="#" data-type="textarea" data-name="description" data-original-title="Описание если нужно">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Управляющий</td>
                        <td><a href="#" id="user-id" data-type="select2" data-source='{!! $users_json !!}' data-name="user_id" data-original-title="Кто управляющий">не указано</a></td>
                    </tr>
                    </tbody>
                </table>
                <button id="save-btn" class="btn btn-primary">Сохранить новый магазин</button>
            </div>
        </div>
    </div>
@stop