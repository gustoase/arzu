@extends('layouts.main')

@section('js')
<script src="/js/store.js"></script>
@stop

@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="col-md-9">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title">Информация о магазине</h3>
                        </div>
                        <div class="panel-body" id="store-info" data-url="{{ route('store', ['storeId' => $store->id]) }}">
                            @if(Auth::user()->is('admin|moderator') || $isOwner)
                                @include('store.info_edit')
                            @else
                                @include('store.info_read')
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-3 statistic-block">
                    <h4>Баланс</h4>
                    <div class="radius-block" data-chart-value="{{$store->balance}}" data-chart-prefix="руб."></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9">
                    @include('store.order_list')
                </div>

                <div class="col-md-3">
                    @include('store.orders_status')
                </div>
            </div>
        </div>
    </div>
@stop