@extends('layouts.main')


@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row placeholders">
                @foreach ($list as $store)
                <div class="col-xs-6 col-sm-3 statistic-block">
                    <h4>{{ $store->name }}</h4>
                    <a href="{{route('store', ['storeId' => $store->id])}}">
                        <div class="radius-block" data-chart-value="{{$store->balance}}" data-chart-prefix="руб."></div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@stop