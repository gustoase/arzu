<div class="row">
    <div class="col-md-3">№ магазина</div>
    <div class="col-md-9">{{ $store->id }}</div>
</div>
<div class="row">
    <div class="col-md-3">Название </div>
    <div class="col-md-9"><a href="#" data-type="text" data-pk="{{ $store->id }}" id="name">{{$store->name}}</a></div>
</div>
<div class="row">
    <div class="col-md-3">Управляющий</div>
    @if(Auth::user()->is('admin|moderator'))
        <div class="col-md-9"><a href="#" data-type="select2" data-source='{!! $users_store !!}' data-pk="{{ $store->id }}" id="user_id">{{$store->user->name}}</a></div>
    @else
        <div class="col-md-9">{{$store->user->name}}</div>
    @endif
</div>
<div class="row">
    <div class="col-md-12"><a href="#" data-type="textarea" data-pk="{{ $store->id }}" id="description">{{$store->description}}</a></div>
</div>