@extends('layouts.main')

@section('js')
<script src="/js/store.js"></script>
@stop

@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="table-responsive">
                <table id="store-stock-product-list-table" class="table table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID товара</th>
                        <th>Название</th>
                        <th>Кол-во</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop