@extends('layouts.main')

@section('js')
<script src="/js/supply.js"></script>
@stop

@section('content')
    <div class="row">
        @include('supply.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="table-responsive">
                <table id="supply-list-table" class="table table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Поставщик</th>
                        <th>На какой склад</th>
                        <th>Дата поставки</th>
                        <th>Товары</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop