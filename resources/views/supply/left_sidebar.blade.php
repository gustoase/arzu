<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li><a href="{{ route('supply_list') }}">Список поставок</a></li>
        @role('admin|moderator')
        <li><a href="{{ route('supply_new') }}">Новая поставка</a></li>
        @endrole
    </ul>
</div>