@extends('layouts.main')

@section('js')
<script src="/js/supply.js"></script>
<script type="application/javascript">
    var productsList = {!! $products_json !!};
</script>
@stop

@section('content')
    <div class="row">
        @include('supply.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="alert alert-danger hide" id="msg" role="alert"></div>
                <table id="new-supply" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td width="18%">Название поставщика</td>
                        <td><a href="#" id="provider" data-type="text" data-name="provider" data-original-title="Кто поставил товар?">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Склад</td>
                        <td><a href="#" id="stock-id" data-type="select2" data-source='{!! $stocks_json !!}' data-name="stock_id" data-original-title="На какой склад">не указано</a></td>
                    </tr>
                    <tr>
                        <td>
                            Продукты
                            <button type="button" class="btn btn-default btn-sm add-product">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Добавить
                            </button>
                        </td>
                        <td>
                            <table class="table table-bordered" id="product-list">
                                <thead>
                                <tr>
                                    <th>Товар</th>
                                    <th>Цена</th>
                                    <th>Кол-во</th>
                                    <th>Действие</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <button id="save-btn" class="btn btn-primary">Сохранить поставку</button>
            </div>
        </div>
    </div>
@stop