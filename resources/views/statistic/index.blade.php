@extends('layouts.main')

@section('js')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="/js/statistic.js"></script>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 main">
            <h1 class="page-header">{{ $title }}</h1>

            <div class="alert alert-success text-center statistic-select-date" role="alert">
                показать от <a href="#" class="date-filter" id="date_start" data-type="combodate" data-title="от">{{ $date_start }}</a>
                до <a href="#" class="date-filter" id="date_end" data-type="combodate" data-title="от">{{ $date_end }}</a>
                <button class="btn btn-danger btn-sm" type="button" id="submit-date">Показать</button>
            </div>

            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Статистика по магазинам за период времени</div>

                <!-- Table -->
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Название</th>
                        <th>Кол-во заказов</th>
                        <th>Сумма денег на отгрузку</th>
                        <th>Сумма денег от продажи</th>
                        <th>Растраты магазина</th>
                        <th>Выручка с учетом растрат магазина</th>
                        <th>Текущий баланс</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total_profit = 0.0; ?>
                    @foreach($storesStatistics as $store)
                    <tr class="statistic-block-info">
                        <th scope="row"><a target="_blank" href="{{ route('store', ['storeId' => $store['store_id']]) }}">{{ $store['store_id'] }}</a></th>
                        <td>{{ $store['store_name'] }}</td>
                        <td>{{ $store['count_orders'] }}</td>
                        <td>{{ $store['sum_price_in_order'] }} руб.</td>
                        <td>{{ $store['sum_price_out_order'] }} руб.</td>
                        <td><a target="_blank" href="{{ route('store_charges', ['storeId' => $store['store_id']]) }}">{{ $store['sum_charges_cost'] }} руб.</a></td>
                        <td>{{ $store['sum_price_profit'] }} руб.</td> <?php $total_profit += $store['sum_price_profit'] ?>
                        <td>{{ $store['balance'] }} руб.</td>
                    </tr>
                    <tr class="statistic-block-chart">
                       <td colspan="8">
                           <div class="chart" data-values='{{ json_encode($store['statistic_with_date_for_chart']) }}'></div>
                       </td>
                    </tr>
                    @endforeach
                    <tr class="warning">
                        <td colspan="6" style="text-align: right">
                            <b>Итого</b>
                        </td>
                        <td colspan="2">
                            <b>{{ $total_profit }} руб.</b>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop