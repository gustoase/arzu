@extends('layouts.auth')

@section('content')

    <form method="POST" action="/auth/register" class="form-auth register">

        {!! csrf_field() !!}

        <h2 class="form-auth-heading">Регистрация точки/магазина</h2>

        <label for="name" class="sr-only">Название точки</label>
        <input type="text" id="name" name="name" class="form-control" placeholder="Название магазина/точки/имя владельца" required autofocus>

        <label for="phone" class="sr-only">Телефон</label>
        <input type="text" id="phone" name="phone" class="form-control" placeholder="Телефон владельца" required>


        <label for="password" class="sr-only">Пароль</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Пароль" required>


        <label for="password_confirmation" class="sr-only">Повторите пароль</label>
        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="Повтор пароля" required>


        <label for="email" class="sr-only"></label>
        <input type="email" id="email" name="email" class="form-control" placeholder="Электронная почта если есть">

        <a href="/auth/login">Войти в систему</a>
        <br/>
        <br/>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Зарегистрироваться</button>
    </form>

@endsection