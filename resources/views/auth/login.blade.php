@extends('layouts.auth')

@section('content')

    <form method="POST" action="/auth/login" class="form-auth">

        {!! csrf_field() !!}

        <h2 class="form-auth-heading">Аторизация</h2>

        <label for="phone" class="sr-only">Телефон</label>
        <input type="text" id="phone" name="phone" class="form-control" placeholder="Телефон владельца" required autofocus>

        <label for="password" class="sr-only">Пароль</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Пароль" required>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember"> Запомнить меня
            </label>
        </div>

        <a href="/auth/register">Зарегистрироваться</a>
        <br>
        <br>

        {{$errors->first('phone_or_pass')}}
        <button class="btn btn-lg btn-primary btn-block" type="submit">Отправить</button>
    </form>

@endsection