<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title }}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/css/lib/bootstrap-editable.css" rel="stylesheet">
    <link href="/css/lib/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/css/lib/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/lib/fakeLoader.css">
    {{--<link href="/css/lib/select2-bootstrap.css" rel="stylesheet">--}}

    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/d3.css" rel="stylesheet">

    @section('css')
    @show

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="overlay"></div>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Учёт+</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                @role('admin|moderator')
                <li><a href="{{ route('statistic') }}">Статистика</a></li>
                @endrole
                <li><a href="{{ route('stock_list') }}">Склад</a></li>
                @role('admin|moderator')
                <li><a href="{{ route('supply_list') }}">Поставки</a></li>
                <li><a href="{{ route('order_list') }}">Заказы
                        <span class="label label-warning" data-toggle="tooltip" data-placement="bottom" title="Новые заказы">{{ Helper::getCountNewOrder() }}</span>
                    </a></li>
                @endrole
                <li><a href="{{ route('product_list') }}">Товары</a></li>
                <li><a href="{{ route('store_list') }}">Магазины</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }}
                        @role('admin')
                            <span class="label label-danger">администратор</span>
                        @endrole
                        @role('moderator')
                            <span class="label label-primary">управляющий</span>
                        @endrole
                        @role('store')
                            <span class="label label-default">магазин</span>
                        @endrole
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/auth/logout">Выход</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="main">
            @yield('content')
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/js/lib/jquery-2.0.3.min.js"></script>
<script src="/js/lib/moment.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/js/lib/bootstrap-editable.min.js"></script>
<script src="/js/lib/jquery.dataTables.min.js"></script>
<script src="/js/lib/dataTables.bootstrap.min.js"></script>
<script src="/js/lib/select2.full.min.js"></script>
<script src="/js/lib/fakeLoader.js"></script>
<script src="/js/lib/d3.min.js"></script>
<script src="/js/d3.radial.js"></script>
<script src="/js/main.js"></script>

@section('js')
@show
</body>
</html>
