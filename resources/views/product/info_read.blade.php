<tr>
    <td>Название товара</td>
    <td>{{ $product->name }}</td>
</tr>
<tr>
    <td>Описание товара</td>
    <td>{{ $product->description }}</td>
</tr>
<tr>
    <td>Кол-во в корбке</td>
    <td>{{ $product->in_box }}</td>
</tr>
<tr>
    <td>Цена товара</td>
    <td>{{ $product->price }}</td>
</tr>