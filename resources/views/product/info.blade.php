@extends('layouts.main')

@section('js')
<script src="/js/product.js"></script>
@stop

@section('content')
    <div class="row">
        @include('product.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">

                @if($product->is_deleted)
                <div class="alert alert-danger" role="alert">
                    <h4>Товар был удален из системы</h4>
                </div>
                @endif

                <table id="product-info" data-url="{{route('product_update')}}" class="table table-striped table-bordered">
                    @role('admin|moderator')
                        @include('product.info_edit')
                    @else
                        @include('product.info_read')
                    @endrole
                </table>
                @role('admin|moderator')
                    @if(!$product->is_deleted)
                        <button data-id="{{ $product->id }}" id="remove-product" type="button" class="btn btn-danger">Удалить</button>
                    @endif
                @endrole
            </div>
        </div>
    </div>
@stop