@extends('layouts.main')

@section('js')
<script src="/js/product.js"></script>
@stop

@section('content')
    <div class="row">
        @include('product.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <table id="product-list-table" class="table table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название товара</th>
                        <th>Описание</th>
                        <th>В упаковке</th>
                        <th>Цена за еденицу</th>
                        <th>Цена за упаковку</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop