@extends('layouts.main')

@section('js')
<script src="/js/product.js"></script>
@stop

@section('content')
    <div class="row">
        @include('stock.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="alert alert-danger hide" id="msg" role="alert"></div>
                <table id="new-product" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td width="40%">Название</td>
                        <td><a href="#" data-type="text" data-name="name" data-original-title="Как называется товар?">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Описание(не обязательно)</td>
                        <td><a href="#" data-type="textarea" data-name="description" data-original-title="Описание если есть">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Какое кол-во в упаковке</td>
                        <td><a href="#" data-type="text" data-name="in_box" data-original-title="Сколько единиц в коробке\упаковке">0</a></td>
                    </tr>
                    <tr>
                        <td>Первоначальная цена</td>
                        <td><a href="#" data-type="text" data-name="price" data-original-title="Сколько стоит?">0.0</a></td>
                    </tr>
                    </tbody>
                </table>
                <button id="save-btn" class="btn btn-primary">Сохранить новый товар</button>
            </div>
        </div>
    </div>
@stop