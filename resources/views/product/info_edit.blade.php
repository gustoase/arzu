<tr>
    <td>Название товара</td>
    <td><a href="#" data-type="text" data-pk="{{ $product->id }}" id="name">{{ $product->name }}</a></td>
</tr>
<tr>
    <td>Описание товара</td>
    <td><a href="#" data-type="textarea" data-pk="{{ $product->id }}" id="description">{{ $product->description }}</a></td>
</tr>
<tr>
    <td>Кол-во в корбке</td>
    <td><a href="#" data-type="text" data-pk="{{ $product->id }}" id="in_box">{{ $product->in_box }}</a></td>
</tr>
<tr>
    <td>Цена товара</td>
    <td><a href="#" data-type="text" data-pk="{{ $product->id }}" id="price">{{ $product->price }}</a></td>
</tr>