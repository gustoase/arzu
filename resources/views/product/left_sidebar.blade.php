<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
{{--        <li class="{{ Route::currentRouteNamed('product_list') ? 'active' : '' }}"><a href="{{ route('product_list') }}">Список товаров</a></li>--}}
        <li><a href="{{route('product_list')}}">Список товаров</a></li>
        @role('admin|moderator')
        <li><a href="{{route('product_new')}}">Добавить товар</a></li>
        @endrole
    </ul>
</div>