<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="row">
    <div style="width: 800px" class="center-block">

        <div class="col-md-12">
            <h2>Заказ №{{ $order->id }} от {{ $order->created_at->toDateString() }}</h2>
            <h4>Магазин «{{ $order->store->name }}»</h4>
        </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Кол-во</th>
                <th>Отдано по цене</th>
                <th>Продано кол-во</th>
                <th>Продано по цене</th>
            </tr>
            </thead>
            <tbody>
            <?php $sumIn = 0.0; $sumOut = 0.0;?>
            @foreach($orderProducts as $key => $orderProduct)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $orderProduct->product->name }}</td>
                <td>{{ $orderProduct->count }}</td>
                <td>{{ $orderProduct->price_in }} ({{ $orderProduct->count * $orderProduct->price_in }}) руб.</td>
                <td>{{ $orderProduct->count_out == 0 ? '________________' : $orderProduct->count_out }}</td>
                <td>{{ $orderProduct->price_out == 0 ? '________________' : $orderProduct->price_out }}руб.</td>
            </tr>

            {{--Подсчет суммы--}}
            <?php $sumIn  += ($orderProduct->price_in  * $orderProduct->count) ?>
            <?php $sumOut += ($orderProduct->price_out * $orderProduct->count_out) ?>
            @endforeach
            <tr class="warning">
                <td colspan="3" class="text-right">Потрачено:</td>
                <td>{{ number_format($sumIn, 2) }}руб.</td>
                <td>Получено: {{ number_format($sumOut, 2) }}руб.</td>
                <td><b>Выручка: {{ number_format($sumOut - $sumIn, 2) }}руб.</b></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>

