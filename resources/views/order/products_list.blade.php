<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Список товаров в заказе №{{ $order->id }} от «{{ $order->store->name }}»</h4>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Название</th>
        <th>Кол-во</th>
        <th>Принято по цене</th>
        <th>Продано по цене</th>
        <th>Проданное кол-во</th>
    </tr>
    </thead>
    <tbody>
    <?php $sumIn = 0.0; $sumOut = 0.0;?>
    @foreach($orderProducts as $orderProduct)
    <tr>
        <td><a target="_blank" href="{{ route('product', ['productId' => $orderProduct->product_id]) }}">{{ $orderProduct->product_id }}</a></td>
        <td>{{ $orderProduct->product->name }}</td>
        @if(Auth::user()->is('admin|moderator'))
            <td>
                <a href="#" class="update-order-product" data-url="{{ route('order_product') }}" data-type="text" data-pk="{{ $orderProduct->id }}" id="count">{{ $orderProduct->count }}</a> шт. c
                {{--выпадающий список со имеющиемся складами, где показывается сколько данного товара на складе--}}
                <a href="#" class="update-order-product-stock" data-url="{{ route('order_product') }}"  data-type="select2" data-name="stock_id" data-pk="{{ $orderProduct->id }}"  data-source='{!! json_encode($stockProducts[$orderProduct->product_id]) !!}' data-original-title="склад">{{ $orderProduct->stock->name }}</a>
                <span class="label label-info" data-toggle="tooltip" data-placement="right" data-original-title="Остаток на складе магазина «{{ $order->store->name }}»">
                    {{ isset($storeStockProducts[$orderProduct->product_id]) ? $storeStockProducts[$orderProduct->product_id] : 0 }}
                </span>
            </td>
            <td>
                <a href="#" class="update-order-product" data-url="{{ route('order_product') }}" data-type="text" data-pk="{{ $orderProduct->id }}" id="price_in">{{ $orderProduct->price_in }}</a>
                {{--Предустановленная цена для товара в магазине--}}
                @if(isset($productsPrice[$orderProduct->product_id]))
                    <span class="label label-info" data-toggle="tooltip" data-placement="right" data-original-title="Установленная цена для магазина «{{ $order->store->name }}»">
                        {{ $productsPrice[$orderProduct->product_id] }}
                    </span>
                @endif
            </td>
        @else
            <td>{{ $orderProduct->count }} шт</td>
            <td>{{ $orderProduct->price_in }}</td>
        @endif
        {{--Подсчет суммы--}}
        <?php $sumIn += ($orderProduct->price_in * $orderProduct->count) ?>

        @if((Auth::user()->is('admin|moderator') || $editable) && $order->status == \App\Order::STATUS_SHIPPED && $order->status != \App\Order::STATUS_COMPLETED)
            <td>
                <a href="#" class="update-order-product" data-url="{{ route('order_product') }}" data-type="text" data-pk="{{ $orderProduct->id }}" id="price_out">{{ $orderProduct->price_out }}</a>
            </td>
            <td>
                <a href="#" class="update-order-product" data-url="{{ route('order_product') }}" data-type="text" data-pk="{{ $orderProduct->id }}" id="count_out">{{ $orderProduct->count_out }}</a>
            </td>
        @else
            <td>{{ $orderProduct->price_out }}</td>
            <td>{{ $orderProduct->count_out }}</td>
        @endif

        {{--Подсчет суммы--}}
        <?php $sumOut += ($orderProduct->price_out * $orderProduct->count_out) ?>
    </tr>
    @endforeach
    <tr class="warning">
        <td colspan="3" class="text-right">Потрачено:</td>
        <td>{{ number_format($sumIn, 2) }}руб</td>
        <td>Получено: {{ number_format($sumOut, 2) }}руб </td>
        <td><b>Выручка: {{ number_format($sumOut - $sumIn, 2) }}руб</b></td>
    </tr>
    </tbody>
</table>
<div class="modal-footer">
    <a target="_blank" href="{{ route('order_print', ['orderId' => $order->id]) }}" class="btn btn-default">
        <span class="glyphicon glyphicon-print" aria-hidden="true"></span> на печать
    </a>
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть окно</button>
    @if($order->status == \App\Order::STATUS_NEW)
    <a class="btn btn-primary" href="{{ route('order_edit', ['orderId' => $order->id]) }}" role="button">Редактировать заказ</a>
        @if(Auth::user()->is('admin|moderator'))
        <button type="button" class="btn btn-danger to-shipped-order">Заказ собран</button>
        @endif
    @endif
    @if($order->status == \App\Order::STATUS_SHIPPED)
    <button type="button" class="btn btn-danger close-order">Завершить заказ</button>
    @endif
</div>