@extends('layouts.main')

@section('js')
<script src="/js/order.js"></script>
<script type="application/javascript">
    var productsList = {!! $products_json !!};
</script>
@stop

@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="alert alert-danger hide" id="msg" role="alert"></div>
                От какого магизина - <a href="#" id="store-id" data-type="select2" data-value="{{ $order->store_id }}" data-source='{!! $store_list_json !!}' data-name="store_id" data-original-title="магазин">{{ $order->store->name }}</a>
                <br/>
                Заметка для заказа - <a href="#" id="description" data-type="textarea" data-value="{{ $order->description }}"  data-name="description" data-original-title="Заметка">@if($order->description) {{ $order->description }} @else не указано @endif</a>
                <br/>
                <table class="table table-bordered" id="edit-order">
                    <thead>
                    <tr>
                        <th>Товар</th>
                        <th>Кол-во</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orderProducts as $product)
                        <tr class="product-item">
                            <td>
                                <a href="#" data-type="select2" class="select2-product" data-value="{{ $product->product_id }}" data-name="product_id" data-original-title="Товар">{{ $product->product->name }}</a>
                            </td>
                            <td>
                                <a href="#" data-type="text"data-value="{{ $product->count }}"  data-name="count" data-original-title="Кол-во">{{ $product->count }}</a>
                            </td>
                            <td>
                                <button type="button" class="btn btn-default btn-xs remove-field">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    удалить
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <button type="button" class="btn btn-primary btn-sm add-product">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Добавить товар
                </button>

                <button id="save-btn" class="btn btn-danger btn-sm pull-right">Сохранить заказ</button>
            </div>
        </div>
    </div>
@stop