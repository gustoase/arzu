@extends('layouts.main')

@section('js')
<script src="/js/order.js"></script>
<script type="application/javascript">
    var productsList = {!! $products_json !!};
</script>
@stop

@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="alert alert-danger hide" id="msg" role="alert"></div>
                От какого магизина - <a href="#" id="store-id" data-type="select2" data-source='{!! $store_list_json !!}' data-name="store_id" data-original-title="магазин">не указано</a>
                <br/>
                Заметка для заказа - <a href="#" id="description" data-type="textarea" data-name="description" data-original-title="Заметка">не указано</a>
                <br/>
                <table class="table table-bordered" id="new-order">
                    <thead>
                    <tr>
                        <th>Товар</th>
                        <th>Кол-во</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                <button type="button" class="btn btn-primary btn-sm add-product">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Добавить товар
                </button>

                <button id="save-btn" class="btn btn-danger btn-sm pull-right">Создать заказ</button>
            </div>
        </div>
    </div>
@stop