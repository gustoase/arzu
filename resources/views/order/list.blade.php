@extends('layouts.main')

@section('js')
<script src="/js/order.js"></script>
@stop

@section('content')
    <div class="row">
        @include('store.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="col-md-9">
                    <div class="table-responsive">
                        <table id="order-list-table" class="table table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ID заказа</th>
                                <th>Статус</th>
                                <th>Описание</th>
                                <th>Дата</th>
                            </tr>
                            </thead>
                        </table>
                        <div class="modal fade order-products-modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    @include('store.orders_status')
                </div>
            </div>
        </div>
    </div>
@stop