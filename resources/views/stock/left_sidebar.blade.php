<div class="col-sm-3 col-md-2 sidebar">
    <ul class="nav nav-sidebar">
        <li class="{{ Route::currentRouteNamed('stock_list') ? 'active' : '' }}"><a href="{{ route('stock_list') }}">Список складов</a></li>
        @role('admin|moderator')
        <li><a href="{{ route('stock_create') }}">Добавить склад</a></li>
        @endrole
    </ul>
</div>