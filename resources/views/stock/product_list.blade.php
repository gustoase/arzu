@extends('layouts.main')

@section('js')
<script src="/js/stock.js"></script>
@stop

@section('content')
    <div class="row">
        @include('stock.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="panel panel-warning">
                <div class="panel-heading panel-toggle">
                    <h3 class="panel-title">Информация о складе</h3>
                </div>
                <div class="panel-body hidden" id="stock-info" data-url="{{ route('stock_list') }}">
                    @role('admin|moderator')
                        @include('stock.info_edit')
                    @else
                        @include('stock.info_read')
                    @endrole
                </div>
            </div>

            <div class="table-responsive">
                <table id="stock-product-list-table" class="table table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID товара</th>
                        <th>Название</th>
                        <th>Склад</th>
                        <th>Кол-во</th>
                        <th>Полных коробок</th>
                        <th>В коробке</th>
                        <th>Цена за еденицу</th>
                        <th>Цена за упаковку</th>
                        <th>Дата последней поставки</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop