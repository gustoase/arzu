@extends('layouts.main')

@section('js')
<script src="/js/stock.js"></script>
@stop

@section('content')
    <div class="row">
        @include('stock.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row">
                <div class="alert alert-danger hide" id="msg" role="alert"></div>
                <table id="new-stock" class="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td width="40%">Название склада</td>
                        <td><a href="#" data-type="text" data-name="name" data-original-title="Как называется склад?">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Адрес склада</td>
                        <td><a href="#" data-type="text" data-name="address" data-original-title="Укажите адрес склада">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Телефон склада</td>
                        <td><a href="#" data-type="text" data-name="phone" data-original-title="Какой телефон привязан к складу?">не указано</a></td>
                    </tr>
                    <tr>
                        <td>Описание (опционально)</td>
                        <td><a href="#" data-type="textarea" data-name="description" data-original-title="Описание если нужно">не указано</a></td>
                    </tr>
                    </tbody>
                </table>
                <button id="save-btn" class="btn btn-primary">Сохранить новый склад</button>
            </div>
        </div>
    </div>
@stop