<div class="row">
    <div class="col-md-3">№ склада</div>
    <div class="col-md-9">{{ $stock->id }}</div>
</div>
<div class="row">
    <div class="col-md-3">Название склада </div>
    <div class="col-md-9">{{$stock->name}}</div>
</div>
<div class="row">
    <div class="col-md-3">Находится по адресу </div>
    <div class="col-md-9">{{$stock->address}}</div>
</div>
<div class="row">
    <div class="col-md-3">Доступен по телефону </div>
    <div class="col-md-9">{{$stock->phone}}</div>
</div>
<div class="row">
    <div class="col-md-12">{{$stock->description}}</div>
</div>