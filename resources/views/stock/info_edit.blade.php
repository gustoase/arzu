<div class="row">
    <div class="col-md-3">№ склада</div>
    <div class="col-md-9">{{ $stock->id }}</div>
</div>
<div class="row">
    <div class="col-md-3">Название склада </div>
    <div class="col-md-9"><a href="#" data-type="text" data-pk="{{ $stock->id }}" id="name">{{$stock->name}}</a></div>
</div>
<div class="row">
    <div class="col-md-3">Находится по адресу </div>
    <div class="col-md-9"><a href="#" data-type="text" data-pk="{{ $stock->id }}" id="address">{{$stock->address}}</a></div>
</div>
<div class="row">
    <div class="col-md-3">Доступен по телефону </div>
    <div class="col-md-9"><a href="#" data-type="text" data-pk="{{ $stock->id }}" id="phone">{{$stock->phone}}</a></div>
</div>
<div class="row">
    <div class="col-md-12"><a href="#" data-type="textarea" data-pk="{{ $stock->id }}" id="description">{{$stock->description}}</a></div>
</div>