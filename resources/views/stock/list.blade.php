@extends('layouts.main')


@section('content')
    <div class="row">
        @include('stock.left_sidebar')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">{{$title}}</h1>

            <div class="row placeholders">
                @foreach ($list as $row)
                <div class="col-xs-6 col-sm-3 statistic-block">
                    <h4>{{ $row['stock']->name }}</h4>
                    <a href="{{route('stock_products_list', ['stock_id' => $row['stock']->id])}}">
                        <div class="radius-block" data-chart-value="{{$row['count_products']}}" data-chart-prefix="шт."></div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@stop