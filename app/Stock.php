<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Склад
 *
 * Class Stock
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $description
 * @property StockProduct[] $stockProducts
 */
class Stock extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = ['name', 'address', 'phone', 'description'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stock';

    public function stockProducts() {
        return $this->hasMany('App\StockProduct');
    }
}
