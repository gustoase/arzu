<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\StockService;
use Illuminate\Http\Response;

class Stock extends Controller
{
    /**
     * @var StockService
     */
    private $stockService;

    /**
     * @param StockService $stockService
     */
    public function __construct(StockService $stockService)
    {
        $this->stockService = $stockService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        $list = $this->stockService->getListStocks();

        $result = [];
        foreach ($list as $stock) {
            $count = $stock->stockProducts()->where('count', '>', 0)->count();
            $result[] = [
                'count_products' => $count,
                'stock' => $stock
            ];
        }

        return view('stock.list', [
            'title' => 'Список доступных складов',
            'list'  => $result
        ]);
    }

    /**
     * Получить список продуктов на складе
     *
     * @param $stockId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function stockProductList($stockId, Request $request) {
        if ($request->isMethod('get')) {
            $stock = $this->stockService->getStock($stockId);

            return view('stock.product_list', [
                'title' => 'Список товаров на складе',
                'stock' => $stock
            ]);
        }

        $page   = (int) $request->input('draw');
        $start  = (int) $request->input('start');
        $length = (int) $request->input('length');
        $search = $request->input('search');

        if (isset($search['value'])) {
            $search = empty($search['value']) ? null : $search['value'];
        } else {
            $search = null;
        }

        $list = $this->stockService->getStockProductList((int) $stockId, $page, $start, $length, $search);

        return response()->json($list);
    }

    /**
     * Обновить информацию о складе
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateStock(Request $request) {
        $stockId = (int) $request->input('pk');
        if (empty($stockId)) {
            return Response::create('Не передан ID, сообщите разработчику', Response::HTTP_NOT_FOUND);
        }

        $name  = $request->input('name');
        $value = $request->input('value');

        $result = $this->stockService->updateStock($stockId, [
            $name => $value
        ]);

        if ($result) {
            return response('Сохранено');
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Добавить новый склад в систему
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createStock(Request $request) {
        if ($request->isMethod('get')) {
            return view('stock.new', [
                'title' => 'Добавить новый склад в систему'
            ]);
        }


        $name  = $request->input('name');
        $address  = $request->input('address');
        $phone  = $request->input('phone');
        $description  = $request->input('description');

        $result = $this->stockService->createStock($name, $address, $phone, $description);

        if ($result !== false) {
            return response()->json([
                'id' => $result->id
            ]);
        } else {
            return response('Не удалось сохранить новый склад', Response::HTTP_BAD_REQUEST);
        }
    }
}
