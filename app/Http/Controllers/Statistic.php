<?php


namespace app\Http\Controllers;

use App\Services\StatisticService;
use Illuminate\Http\Request;

/**
 * Контроллер по статистике компании
 *
 * Class Statistic
 * @package app\Http\Controllers
 */
class Statistic extends Controller
{
    /**
     * @var StatisticService
     */
    private $statisticService;

    /**
     * @param StatisticService $statisticService
     */
    public function __construct(StatisticService $statisticService)
    {
        $this->statisticService = $statisticService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $dateStart = $request->input('date_start', date('Y-m-d', strtotime('-1 months')));
        $dateEnd   = $request->input('date_end',   date('Y-m-d'));

        $storesStatistics = $this->statisticService->getStatisticOrdersStore($dateStart, $dateEnd);

        return view('statistic.index', [
            'title'             => 'Статистика',
            'date_start'        => $dateStart,
            'date_end'          => $dateEnd,
            'storesStatistics'  => $storesStatistics
        ]);
    }
}