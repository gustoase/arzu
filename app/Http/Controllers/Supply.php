<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use App\Services\StockService;
use App\Services\SupplyService;
use App\SupplyProduct;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class Supply
 * @package App\Http\Controllers
 */
class Supply extends Controller
{
    /**
     * @var SupplyService
     */
    private $supplyService;

    /**
     * @var StockService
     */
    private $stockService;

    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @param SupplyService $supplyService
     * @param StockService $stockService
     * @param ProductService $productService
     */
    public function __construct(SupplyService $supplyService,
                                StockService $stockService,
                                ProductService $productService)
    {
        $this->supplyService  = $supplyService;
        $this->stockService   = $stockService;
        $this->productService = $productService;
    }


    /**
     * Получить список поставок
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getSupplyList(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('supply.list', [
                'title' => 'Список поставок на склады'
            ]);
        }

        $page   = (int)$request->input('draw');
        $start  = (int)$request->input('start');
        $length = (int)$request->input('length');
        $search = $request->input('search');

        if (isset($search['value'])) {
            $search = empty($search['value']) ? null : $search['value'];
        } else {
            $search = null;
        }

        $list = $this->supplyService->getAllSupply($page, $start, $length, $search);

        return response()->json($list);
    }

    /**
     * Создаем поставку
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     */
    public function createSupply(Request $request)
    {
        if ($request->isMethod('get')) {
            $stocks = $this->prepareStocksForSelect();
            $products = $this->prepareProductsForSelect();

            return view('supply.new', [
                'title'         => 'Новая поставка товара на склад',
                'stocks_json'   => json_encode($stocks),
                'products_json' => json_encode($products),
            ]);
        }

        $stockId  = (int) $request->input('stock_id');
        $provider = $request->input('provider');
        $products = $request->input('products');

        if (empty($stockId) || empty($products)) {
            return response('Не указано название поставщика или склад', Response::HTTP_BAD_REQUEST);
        }

        $products = $this->prepareProductsFromPost($products);
        $result = $this->supplyService->createSupply($stockId, $provider, $products);

        if ($result !== false) {
            return response()->json([
                'id' => $result->id
            ]);
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Собираем готовые модельки продуктов в поставке
     *
     * @param array $products
     * @return SupplyProduct[]
     */
    protected function prepareProductsFromPost(array $products) {
        $result = [];
        foreach ($products as $row) {
            $product = new SupplyProduct();
            $product->product_id = (int) $row['product_id'];
            $product->count      = (int) $row['count'];
            $product->price      = (float) str_replace(',', '.', $row['price']);

            $result[] = $product;
        }

        return $result;
    }

    /**
     * Подготовить данные для select2 x-editable
     *
     * @return array
     */
    protected function prepareStocksForSelect()
    {
        $list = $this->stockService->getListStocks();

        $result = [];
        foreach ($list as $stock) {
            $result[] = [
                'id'   => $stock->id,
                'text' => $stock->name
            ];
        }

        return $result;
    }

    /**
     * Подготовить данные для select2 x-editable
     *
     * @return array
     */
    protected function prepareProductsForSelect()
    {
        $list = $this->productService->getAllProducts(1, 0, 500);

        $result = [];
        foreach ($list['data'] as $product) {
            $result[] = [
                'id'   => $product['id'],
                'text' => $product['name']
            ];
        }

        return $result;
    }
}
