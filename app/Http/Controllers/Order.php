<?php

namespace app\Http\Controllers;

use App\OrderProduct;
use App\Services\OrderService;
use App\Services\ProductService;
use App\Services\StoreService;
use App\Stock;
use App\StockProduct;
use App\Store;
use App\StoreProduct;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Order as OrderModel;
use Auth;

/**
 * Класс отвечающий за заказы
 *
 * Class Order
 * @package app\Http\Controllers
 */
class Order extends Controller
{

    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * @var StoreService
     */
    private $storeService;

    /**
     * @param OrderService $orderService
     * @param StoreService $storeService
     */
    public function __construct(OrderService $orderService, StoreService $storeService)
    {
        $this->orderService = $orderService;
        $this->storeService = $storeService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getOrderProducts(Request $request)
    {
        $orderId = (int) $request->input('order_id');
        /** @var OrderModel $order */
        $order = OrderModel::findOrNew($orderId);

        $orderProducts = $this->orderService->getOrderProducts($orderId);

        $orderProductsId = $this->getIdsOrderProducts($orderProducts);

        // кол-во товара на складе магазина
        $storeStockProducts = $this->getStoreStockProducts($order->store_id);
        // кол-во товара на складах вообще
        $stockProducts = $this->getStocksProducts($orderProductsId);
        // цена товара привязанная к данному магазину
        $productsPrice = $this->storeService->getStorePriceProducts($order->store_id, $orderProductsId);

        // владелец ли авторизованный пользователь
        $isOwner = $order->store->user_id === Auth::user()->id;
        $editable = $isOwner && $order->status != OrderModel::STATUS_COMPLETED;

        return view('order.products_list', [
            'order'              => $order,
            'editable'           => $editable,
            'orderProducts'      => $orderProducts,
            'stockProducts'      => $stockProducts,
            'storeStockProducts' => $storeStockProducts,
            'productsPrice'      => $productsPrice
        ]);
    }

    /**
     * Получить список продуктов на складе магазина с ключами id продукта, и кол-во
     *
     * @param int $storeId
     * @return array
     */
    protected function getStoreStockProducts($storeId) {
        /** @var Store $store */
        $store = Store::findOrNew($storeId);
        $storeProducts = $store->storeProducts;

        if (!$storeProducts) {
            return [];
        }

        $result = [];
        /** @var StoreProduct $storeProduct */
        foreach ($store->storeProducts as $storeProduct) {
            $result[$storeProduct->product_id] = $storeProduct->count;
        }

        return $result;
    }

    /**
     * Получить id товаров которые в заказе
     *
     * @param OrderProduct[] $orderProducts
     * @return array
     */
    protected function getIdsOrderProducts($orderProducts) {
        $ids = [];

        foreach ($orderProducts as $orderProduct) {
            $ids[] = $orderProduct->product_id;
        }

        return $ids;
    }

    /**
     * Получить кол-во переденных товаров на складах
     *
     * @param array $orderProductsId
     * @return array
     */
    protected function getStocksProducts(array $orderProductsId) {
        $stockList = [];

        $stockProducts = StockProduct::whereIn('product_id', $orderProductsId)->get();

        /** @var StockProduct $stockProduct */
        foreach ($stockProducts as $stockProduct) {
            if (!isset($stockList[$stockProduct->product_id])) {
                $stockList[$stockProduct->product_id] = [];
            }

            $stockList[$stockProduct->product_id][] = [
                'id'    => $stockProduct->stock_id,
                'text'  => $stockProduct->stock->name . ' - ' . $stockProduct->count . ' шт.'
            ];
        }

        return $stockList;
    }

    /**
     * Обновить информацию о товаре в заказе, например цену продажы
     *
     * @param Request $request
     * @return Response
     */
    public function updateOrderProduct(Request $request)
    {
        $orderProductId = (int)$request->input('pk');
        if (empty($orderProductId)) {
            return response('Не передан ID, сообщите разработчику', Response::HTTP_NOT_FOUND);
        }

        $name  = $request->input('name');
        $value = $request->input('value');

        $result = $this->orderService->updateOrderProduct($orderProductId, [
            $name => $value
        ]);

        if ($result) {
            return response('Сохранено');
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Обновить информацию о заказе, например перевести в другой статус
     *
     * @param Request $request
     * @return Response
     */
    public function updateOrder(Request $request)
    {
        $orderId = (int)$request->input('pk');
        if (empty($orderId)) {
            return response('Не передан ID, сообщите разработчику', Response::HTTP_NOT_FOUND);
        }

        $name = $request->input('name');
        $value = $request->input('value');

        $result = $this->orderService->updateOrder($orderId, Auth::user(), [
            $name => $value
        ]);

        if ($result) {
            return response('Сохранено');
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Создать заказ
     *
     * @param Request $request
     *
     * @param ProductService $productService
     * @param StoreService $storeService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createOrder(Request $request, ProductService $productService, StoreService $storeService)
    {
        if ($request->isMethod('get')) {
            $products = $this->prepareProductsForSelect($productService);
            $storeList = $this->prepareStoreForSelect($storeService);

            return view('order.new', [
                'title'           => 'Новый заказ',
                'products_json'   => json_encode($products),
                'store_list_json' => json_encode($storeList),
            ]);
        }

        $storeId = (int)$request->input('storeId');
        $description = $request->input('description');
        $products = $request->input('products');

        if (empty($storeId) || empty($products) || $storeId == 0) {
            return response('Не указаны магазина или товары', Response::HTTP_BAD_REQUEST);
        }

        $products = $this->prepareProductsFromPost($products);
        $result = $this->orderService->createOrder($storeId, $description, $products);

        if ($result !== false) {
            return response()->json([
                'id' => $result->id
            ]);
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Редактировать заказ
     *
     * @param int $orderId
     * @param Request $request
     * @param ProductService $productService
     * @param StoreService $storeService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editOrder($orderId, Request $request, ProductService $productService, StoreService $storeService)
    {
        $orderId = (int)$orderId;

        $order = OrderModel::findOrNew($orderId);

        if ($order->status !== OrderModel::STATUS_NEW && !Auth::user()->is('admin|moderator')) {
            return redirect()->route('order_new');
        }

        if ($request->isMethod('get')) {
            $products = $this->prepareProductsForSelect($productService);
            $storeList = $this->prepareStoreForSelect($storeService);
            $orderProducts = $this->orderService->getOrderProducts($orderId);

            return view('order.edit', [
                'title'           => 'Редактировать заказ №' . $orderId,
                'products_json'   => json_encode($products),
                'store_list_json' => json_encode($storeList),
                'orderProducts'   => $orderProducts,
                'order'           => $order
            ]);
        }

        $storeId     = (int)$request->input('storeId');
        $description = $request->input('description');
        $products    = $request->input('products');

        if (empty($products || $storeId == 0)) {
            return response('Не указаны магазина или товары', Response::HTTP_BAD_REQUEST);
        }

        $products = $this->prepareProductsFromPost($products);
        $result = $this->orderService->editOrder($orderId, $storeId, $description, $products);

        if ($result !== false) {
            return response()->json([
                'id' => $result->id
            ]);
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Получить список заказов для админа
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListOrder(Request $request)
    {
        if ($request->isMethod('get')) {
            $countNotCompleted = $this->orderService->getCountNotCompleted(null);
            $countCompleted    = $this->orderService->getCountCompleted(null);

            return view('order.list', [
                'title'             => 'Список заказов',
                'countNotCompleted' => $countNotCompleted,
                'countCompleted'    => $countCompleted
            ]);
        }

        $page   = (int)$request->input('draw');
        $start  = (int)$request->input('start');
        $length = (int)$request->input('length');

        //какие заказы показывать not_completed - не завершенные, и completed - завершенные
        $filterList = $request->input('filter', 'not_completed');

        $list = $this->orderService->getListOrder(null, $filterList, $page, $start, $length);

        return response()->json($list);
    }

    /**
     * Собираем готовые модельки продуктов в поставке
     *
     * @param array $products
     * @return OrderProduct[]
     */
    protected function prepareProductsFromPost(array $products)
    {
        /** @var Stock $firstStock */
        $firstStock = Stock::first();
        $result = [];
        foreach ($products as $row) {
            $product = new OrderProduct();
            $product->product_id = (int)$row['product_id'];
            $product->stock_id = $firstStock->id;
            $product->count = (int)$row['count'];

            $result[] = $product;
        }

        return $result;
    }

    /**
     * Подготовить данные для select2 x-editable
     *
     * @param ProductService $productService
     * @return array
     */
    protected function prepareProductsForSelect(ProductService $productService)
    {
        $list = $productService->getAllProducts(1, 0, 500);

        $result = [];
        foreach ($list['data'] as $product) {
            $result[] = [
                'id'   => $product['id'],
                'text' => $product['name']
            ];
        }

        return $result;
    }

    /**
     * Подготовить данные для select2 x-editable
     *
     * @param StoreService $storeService
     * @return array
     */
    protected function prepareStoreForSelect(StoreService $storeService)
    {
        $user = Auth::user();

        if ($user->is('admin|moderator')) {
            $userId = null;
        } else {
            $userId = $user->id;
        }

        $list = $storeService->getListStore($userId);

        $result = [];
        /** @var \App\Store $store */
        foreach ($list as $store) {
            $result[] = [
                'id'   => $store->id,
                'text' => $store->name
            ];
        }

        return $result;
    }

    /**
     * Распечатать заказ
     *
     * @param int $orderId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function printOrder($orderId)
    {
        /** @var OrderModel $order */
        $order = OrderModel::findOrNew($orderId);

        if (!Auth::user()->is('admin|moderator') && Auth::user()->id !== $order->store->user_id) {
            return redirect()->route('store_list');
        }

        $orderProducts = $this->orderService->getOrderProducts($orderId);

        return view('order.print', [
            'order'         => $order,
            'orderProducts' => $orderProducts
        ]);
    }
}