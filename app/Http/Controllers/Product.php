<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;

/**
 * Class Product
 * @package App\Http\Controllers
 */
class Product extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param int $productId
     * @return \Illuminate\View\View
     */
    public function getProduct($productId) {
        $result = $this->productService->getProduct($productId);

        return view('product.info', [
            'title' => 'Просмотр и редактирование товара',
            'product' => $result
        ]);
    }

    /**
     * Обновление продукта посредством x-editor
     * @param Request $request
     * @return Response
     */
    public function updateProduct(Request $request) {
        $productId = (int) $request->input('pk');
        if (empty($productId)) {
            return Response::create('Не передан ID, сообщите разработчику', Response::HTTP_NOT_FOUND);
        }

        $name  = $request->input('name');
        $value = $request->input('value');

        $result = $this->productService->updateProduct($productId, [
            $name => $value
        ]);

        if ($result) {
            return Response::create('Сохранено');
        } else {
            return Response::create('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Удаление продукта
     *
     * @param int $productId
     * @return Response
     */
    public function removeProduct($productId) {
        $result = $this->productService->removeProduct((int) $productId);

        if ($result) {
            return Response::create('Удалено');
        } else {
            return Response::create('Не удалось удалить', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Получить список продуктов
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getProductList(Request $request) {
        if ($request->isMethod('get')) {
            return view('product.list', [
                'title' => 'Полный список товаров'
            ]);
        }

        $page   = (int) $request->input('draw');
        $start  = (int) $request->input('start');
        $length = (int) $request->input('length');

        $list = $this->productService->getAllProducts($page, $start, $length);

        return response()->json($list);
    }

    /**
     * Добавить новый товар в систему
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createProduct(Request $request) {
        if ($request->isMethod('get')) {
            return view('product.new', [
                'title' => 'Добавить новый товар в систему'
            ]);
        }


        $name   = $request->input('name');
        $inBox  = (int) $request->input('in_box');
        $price  = (float) str_replace(',', '.', $request->input('price'));
        $description  = $request->input('description');

        if (empty($name) || empty($price)) {
            return response('Не указана цена или название', Response::HTTP_BAD_REQUEST);
        }

        $result = $this->productService->createProduct($name, $price, $inBox, $description);

        if ($result !== false) {
            return response()->json([
                'id' => $result->id
            ]);
        } else {
            return response('Не удалось сохранить новый товар', Response::HTTP_BAD_REQUEST);
        }
    }
}
