<?php


namespace app\Http\Controllers;

use App\Services\OrderService;
use App\Services\ProductService;
use App\Services\StoreService;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Контроллер отвечающий за магазины
 *
 * Class Store
 * @package app\Http\Controllers
 */
class Store extends Controller
{

    /**
     * Минимальная сумма в рублях для расстраты
     */
    const MIN_CHARGE_SUM = 10;

    /**
     * @var StoreService
     */
    protected $storeService;

    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * @param StoreService $storeService
     * @param OrderService $orderService
     */
    public function __construct(StoreService $storeService, OrderService $orderService)
    {
        $this->storeService = $storeService;
        $this->orderService = $orderService;
    }


    /**
     * Список магазинов
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStoreList()
    {
        $userId = Auth::user()->id;

        if (Auth::user()->is('admin|moderator')) {
            $userId = null;
        }
        $list = $this->storeService->getListStore($userId);

        return view('store.list', [
            'title' => 'Список магазинов пользователя ' . Auth::user()->name,
            'list'  => $list
        ]);
    }

    /**
     * Карточка магазина
     *
     * @param int $storeId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStore($storeId)
    {
        $store = $this->storeService->getStore($storeId);

        // владелец ли авторизованный пользователь
        $isOwner = $store->user_id === Auth::user()->id;

        if (!$isOwner && !Auth::user()->is('admin|moderator')) {
            return redirect()->route('store_list');
        }

        $countNotCompleted = $this->orderService->getCountNotCompleted($storeId);
        $countCompleted = $this->orderService->getCountCompleted($storeId);

        return view('store.info', [
            'title'             => 'Карточка магазина «' . $store->name . '»',
            'store'             => $store,
            'isOwner'           => $isOwner,
            'countNotCompleted' => $countNotCompleted,
            'countCompleted'    => $countCompleted,
            'users_store'       => json_encode($this->prepareUsersForSelect())
        ]);
    }

    /**
     * Получить список заказов в магазине
     *
     * @param int $storeId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListOrder($storeId, Request $request)
    {
        $page   = (int)$request->input('draw');
        $start  = (int)$request->input('start');
        $length = (int)$request->input('length');
        //какие заказы показывать not_completed - не завершенные, и completed - завершенные
        $filterList = $request->input('filter', 'not_completed');

        $list = $this->orderService->getListOrder((int)$storeId, $filterList, $page, $start, $length);

        return response()->json($list);
    }


    /**
     * Обновить информацию о складе
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateStore(Request $request)
    {
        $storeId = (int)$request->input('pk');
        if (empty($storeId)) {
            return response('Не передан ID, сообщите разработчику', Response::HTTP_NOT_FOUND);
        }

        $name  = $request->input('name');
        $value = $request->input('value');

        $result = $this->storeService->updateStore($storeId, [
            $name => $value
        ]);

        if ($result) {
            return response()->json([
                'status' => 'Сохранено'
            ]);
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Добавить новый магазин в систему
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createStore(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('store.new', [
                'title'      => 'Добавить новый магазин в систему',
                'users_json' => json_encode($this->prepareUsersForSelect())
            ]);
        }

        $userId = (int)$request->input('user_id');
        $name   = $request->input('name');
        $description = $request->input('description');

        $result = $this->storeService->createStore($userId, $name, $description);

        if ($result !== false) {
            return response()->json([
                'id' => $result->id
            ]);
        } else {
            return response('Не удалось сохранить новый магазин', Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Подготовить данные для select2 x-editable
     *
     * @return array
     */
    protected function prepareUsersForSelect()
    {
        $list = User::all();

        $result = [];
        /** @var User $user */
        foreach ($list as $user) {
            $result[] = [
                'id'   => $user->id,
                'text' => $user->name
            ];
        }

        return $result;
    }



    /**
     * Редактировать цены на товары для магазина
     *
     * @param int $storeId
     * @param Request $request
     * @param ProductService $productService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getListProductPrice($storeId, Request $request, ProductService $productService)
    {
        $storeId = (int)$storeId;

        /** @var \App\Store $store */
        $store = \App\Store::findOrNew($storeId);

        if ($request->isMethod('get')) {
            $products = $this->prepareProductsWithForSelect($productService);
            $productPriceList = $this->storeService->getListProductPrice($storeId);

            return view('store.product_price', [
                'title'            => 'Цены на товары для магазина ' . $store->name,
                'products_json'    => json_encode($products),
                'productPriceList' => $productPriceList,
                'store'            => $store
            ]);
        }

        $products = $request->input('products');

        if (empty($products)) {
            return response('Не верно заполнили поля', Response::HTTP_BAD_REQUEST);
        }

        $result = $this->storeService->addOrUpdateProductPrice($storeId, $products);

        if ($result) {
            return response('Сохранено');
        } else {
            return response('Не удалось сохранить', Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * Подготовить данные для select2 x-editable c ценами на складе
     *
     * @param ProductService $productService
     * @return array
     */
    protected function prepareProductsWithForSelect(ProductService $productService)
    {
        $list = $productService->getAllProducts(1, 0, 500);

        $result = [];
        foreach ($list['data'] as $product) {
            $result[] = [
                'id'   => $product['id'],
                'text' => $product['name'] . ' - ' . $product['price'] . ' руб.'
            ];
        }

        return $result;
    }

    /**
     * Что есть на складе магазина
     *
     * @param int $storeId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getListStoreStock($storeId, Request $request)
    {
        $storeId = (int) $storeId;
        if ($request->isMethod('get')) {
            $store = $this->storeService->getStore($storeId);

            return view('store.product_list', [
                'title' => 'Список товаров на складе магазина «' . $store->name . '»',
                'store' => $store
            ]);
        }

        $page   = (int) $request->input('draw');
        $start  = (int) $request->input('start');
        $length = (int) $request->input('length');

        $list = $this->storeService->getStoreStockProducts($storeId, $page, $start, $length);

        return response()->json($list);
    }

    /**
     * Список растрат магазина + фиксация растраты
     *
     * @param int $storeId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getListCharges($storeId, Request $request)
    {
        $storeId = (int) $storeId;
        if ($request->isMethod('get')) {
            $store = $this->storeService->getStore($storeId);
            $chargesType = $this->prepareChargesTypeForSelect();

            return view('store.charges_list', [
                'title'             => 'Список дополнительных растрат магазина «' . $store->name . '»',
                'store'             => $store,
                'charges_type_json' => json_encode($chargesType)
            ]);
        }


        // новый расход магазина
        $isNewCharge = (int) $request->input('new_charge', 0);
        if ($isNewCharge == 1) {
            $description  = $request->input('description');
            $chargeTypeId = (int) $request->input('charge_type_id');
            $cost         = (float) $request->input('cost');

            if ($cost == 0 || $cost < self::MIN_CHARGE_SUM || $chargeTypeId == 0) {
                return response('Укажите правильно сумму или тип', Response::HTTP_BAD_REQUEST);
            }

            $result = $this->storeService->newChargesStore($storeId, $chargeTypeId, $cost, $description);

            if ($result) {
                return response()->json(['status' => 'успешно добавлено']);
            } else {
                return response('Произошла ошибка добавления', Response::HTTP_BAD_REQUEST);
            }
        }

        $page   = (int) $request->input('draw');
        $start  = (int) $request->input('start');
        $length = (int) $request->input('length');

        $list = $this->storeService->getStoreChargesList($storeId, $page, $start, $length);

        return response()->json($list);
    }


    /**
     * Подготовить данные для select2 x-editable типы растрат
     *
     * @return array
     */
    protected function prepareChargesTypeForSelect()
    {
        $charges = $this->storeService->getAllChargesType();

        $result = [];
        foreach ($charges as $charge) {
            $result[] = [
                'id'   => $charge->id,
                'text' => $charge->name
            ];
        }

        return $result;
    }
}