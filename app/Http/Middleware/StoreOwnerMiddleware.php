<?php

namespace App\Http\Middleware;

use App\Order;
use App\User;
use Closure;
use Auth;

class StoreOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        $orderId = $request->input('order_id');
        $storeId = $request->storeId;

        /** @var User $user */
        $user = Auth::user();

        if ($user->is('admin|moderator'))  {
            return $next($request);
        }

        if (!is_null($orderId)) {
            /** @var Order $order */
            $order = Order::findOrNew($orderId);
            if (!$order) {
                return redirect()->route('store_list');
            }

            if ($order->store->user_id === $user->id) {
                return $next($request);
            }
        }

        return $next($request);
    }
}
