<?php

Route::controllers([
    'auth'     => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);


Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'Dashboard@index');

    // склады
    Route::get('/stock', ['as' => 'stock_list', 'uses' => 'Stock@index']);
    Route::get('/stock/new', ['as' => 'stock_create', 'middleware' => 'role:admin|moderator', 'uses' => 'Stock@createStock']);
    Route::post('/stock/new', ['as' => 'stock_create', 'middleware' => 'role:admin|moderator', 'uses' => 'Stock@createStock']);
    Route::put('/stock', ['as' => 'stock_list', 'middleware' => 'role:admin|moderator', 'uses' => 'Stock@updateStock']);
    Route::get('/stock/{stockId}/products', ['as' => 'stock_products_list', 'uses' => 'Stock@stockProductList']);
    Route::post('/stock/{stockId}/products', ['as' => 'stock_products_list', 'uses' => 'Stock@stockProductList']);

    // продукты
    Route::put('/product/update', ['as' => 'product_update', 'middleware' => 'role:admin|moderator', 'uses' => 'Product@updateProduct']);
    Route::get('/product/list', ['as' => 'product_list', 'uses' => 'Product@getProductList']);
    Route::post('/product/list', ['as' => 'product_list', 'uses' => 'Product@getProductList']);
    Route::get('/product/new', ['as' => 'product_new', 'middleware' => 'role:admin|moderator', 'uses' => 'Product@createProduct']);
    Route::post('/product/new', ['as' => 'product_new', 'middleware' => 'role:admin|moderator', 'uses' => 'Product@createProduct']);
    Route::delete('/product/{productId}', ['as' => 'product', 'middleware' => 'role:admin|moderator', 'uses' => 'Product@removeProduct']);
    Route::get('/product/{productId}', ['as' => 'product', 'uses' => 'Product@getProduct']);

    // поставки
    Route::get('/supply/list', ['as' => 'supply_list','middleware' => 'role:admin|moderator', 'uses' => 'Supply@getSupplyList']);
    Route::post('/supply/list', ['as' => 'supply_list','middleware' => 'role:admin|moderator', 'uses' => 'Supply@getSupplyList']);
    Route::get('/supply/new', ['as' => 'supply_new','middleware' => 'role:admin|moderator', 'uses' => 'Supply@createSupply']);
    Route::post('/supply/new', ['as' => 'supply_new','middleware' => 'role:admin|moderator', 'uses' => 'Supply@createSupply']);

    // магазин
    Route::get('/store/list', ['as' => 'store_list', 'uses' => 'Store@getStoreList']);
    Route::get('/store/new', ['as' => 'store_create', 'middleware' => 'role:admin|moderator',  'uses' => 'Store@createStore']);
    Route::post('/store/new', ['as' => 'store_create', 'middleware' => 'role:admin|moderator',  'uses' => 'Store@createStore']);
    Route::get('/store/{storeId}', ['as' => 'store', 'uses' => 'Store@getStore']);
    Route::put('/store/{storeId}', ['as' => 'store', 'uses' => 'Store@updateStore']);
    Route::post('/store/{storeId}', ['as' => 'store', 'uses' => 'Store@getListOrder']);
    Route::get('/store/{storeId}/product_price', ['as' => 'store_produce_price', 'middleware' => 'role:admin|moderator', 'uses' => 'Store@getListProductPrice']);
    Route::post('/store/{storeId}/product_price', ['as' => 'store_produce_price', 'middleware' => 'role:admin|moderator', 'uses' => 'Store@getListProductPrice']);
    Route::get('/store/{storeId}/stock', ['as' => 'store_stock', 'uses' => 'Store@getListStoreStock']);
    Route::post('/store/{storeId}/stock', ['as' => 'store_stock', 'uses' => 'Store@getListStoreStock']);
    Route::get('/store/{storeId}/charges', ['as' => 'store_charges', 'uses' => 'Store@getListCharges']);
    Route::post('/store/{storeId}/charges', ['as' => 'store_charges', 'uses' => 'Store@getListCharges']);


    // заказы
    Route::get('/order/products', ['as' => 'order_products', 'uses' => 'Order@getOrderProducts']);
    Route::put('/order/product',  ['as' => 'order_product', 'uses' => 'Order@updateOrderProduct']);
    Route::put('/order',  ['as' => 'order', 'uses' => 'Order@updateOrder']);
    Route::get('/order/new',  ['as' => 'order_new', 'uses' => 'Order@createOrder']);
    Route::post('/order/new',  ['as' => 'order_new', 'uses' => 'Order@createOrder']);
    Route::get('/order/list',  ['as' => 'order_list', 'uses' => 'Order@getListOrder']);
    Route::post('/order/list',  ['as' => 'order_list', 'uses' => 'Order@getListOrder']);
    Route::get('/order/{orderId}',  ['as' => 'order_edit', 'uses' => 'Order@editOrder']);
    Route::post('/order/{orderId}',  ['as' => 'order_edit', 'uses' => 'Order@editOrder']);
    Route::get('/order/{orderId}/print',  ['as' => 'order_print', 'uses' => 'Order@printOrder']);

    // отчеты
    Route::get('/statistic', ['as' => 'statistic', 'middleware' => 'role:admin|moderator',  'uses' => 'Statistic@index']);

});
