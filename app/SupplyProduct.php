<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Поставка на склад, какой продукт в какой поставке
 *
 * Class SupplyProduct
 * @package App
 *
 * @property int $id
 * @property int $supply_id
 * @property int $product_id
 * @property int $count
 * @property float $price
 * @property string $created_at
 * @property Supply $supply
 * @property Product $product
 */
class SupplyProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'supply_product';

    public function supply() {
        return $this->belongsTo('App\Supply');
    }

    // Получить связанный продукт
    public function product() {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }
}
