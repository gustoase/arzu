<?php

/**
 * Помощник для вьюх
 *
 * Class Helper
 */
class Helper
{

    /**
     * Получить кол-во новых заказов
     *
     * @return int
     */
    public static function getCountNewOrder()
    {
        $orderService = app()->make('App\Services\OrderService');

        $count = $orderService->getCountNew(null);

        return $count;
    }

}