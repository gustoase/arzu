<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Товары на складе
 *
 * Class StockProduct
 * @package App
 *
 * @property int $id
 * @property int $stock_id
 * @property int $product_id
 * @property float $price
 * @property int $count
 * @property int $last_supply_id
 * @property Product $product
 * @property Stock $stock
 * @property Supply $supply
 */
class StockProduct extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = ['stock_id', 'description', 'in_box', 'price'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stock_product';

    // Получить связанный продукт
    public function product() {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

    // получить последнию поставку этого продукта
    public function supply() {
        return $this->hasOne('App\Supply', 'id', 'last_supply_id');
    }

    // получить объект склада где находится этот продукт
    public function stock() {
        return $this->belongsTo('App\Stock');
    }

    /**
     * Сколько полных упаковок осталось
     * @return float
     */
    public function boxCount() {
        return floor($this->count / $this->product->in_box);
    }

    /**
     * Цена за упаковку
     * @return float
     */
    public function boxPrice() {
        return $this->price * $this->product->in_box;
    }
}
