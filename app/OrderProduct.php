<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Товары указанные в заказе
 *
 * Class OrderProduct
 * @package App
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $stock_id // с какого склада выдали
 * @property int $count
 * @property int $count_out // сколько было продано
 * @property float $price_in
 * @property float $price_out
 * @property Order $order
 */
class OrderProduct extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = ['order_id', 'product_id', 'stock_id', 'count', 'count_out', 'price_in', 'price_out'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_product';

    public function order() {
        return $this->belongsTo('App\Order');
    }

    public function product() {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

    public function stock() {
        return $this->hasOne('App\Stock', 'id', 'stock_id');
    }
}
