<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Фиксированные цена на конкретные товары конкретному магазину,
 * например магазину №1 пакет можно отдать за 10р со склада
 *
 * Class StoreProductPrice
 * @package App
 *
 * @property int $id
 * @property int $store_id
 * @property int $product_id
 * @property float $price
 * @property string $created_at
 */
class StoreProductPrice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_product_price';

    protected $fillable = ['product_id', 'price'];

    // Получить связанный продукт
    public function product() {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }
}
