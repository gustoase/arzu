<?php

namespace App\Services;

use App\ChargesType;
use App\Store;
use App\StoreCharges;
use App\StoreProduct;
use App\StoreProductPrice;
use Log;

/**
 * Сервис отвечающий за работу с магазинами
 *
 * Class StoreService
 * @package App\Services
 */
class StoreService extends BaseService
{

    /**
     * Получить список магазинов пользователя
     *
     * @param int $userId
     * @return Store[]
     */
    public function getListStore($userId)
    {
        if (is_null($userId)) {
            $result = Store::all();
        } else {
            $result = Store::where(['user_id' => $userId])->get();
        }

        return $result;
    }

    /**
     * Список типов растрат
     *
     * @return ChargesType[]
     */
    public function getAllChargesType(){
        $result = ChargesType::all();

        return $result;
    }

    /**
     * Получить информацию о магазине
     *
     * @param int $storeId
     * @return Store
     */
    public function getStore($storeId)
    {
        $store = Store::findOrNew($storeId);

        return $store;
    }

    /**
     * @param int $storeId
     * @param array $fields ['name' => 'new value']
     * @return bool|int
     */
    public function updateStore($storeId, array $fields)
    {
        $this->log(__METHOD__, func_get_args());

        $store = Store::find($storeId);
        $result = $store->update($fields);

        return $result;
    }

    /**
     * Создаем новый магазин
     *
     * @param int $userId
     * @param string $name
     * @param string $description
     *
     * @return Store|bool
     */
    public function createStore($userId, $name, $description)
    {
        $this->log(__METHOD__, func_get_args());

        $store = new Store();
        $store->user_id = $userId;
        $store->name = $name;
        $store->description = $description;

        $result = $store->save();

        if ($result) {
            return $store;
        } else {
            return false;
        }
    }


    /**
     * Получить список товаров которые находятся на складе магазина
     *
     * @param int $storeId
     * @param int $page какую страницу запросили
     * @param int $start
     * @param int $limit сколько выводится на странице
     * @return array
     */
    public function getStoreStockProducts($storeId, $page = 1, $start = 0, $limit = 10)
    {
        $storeId = (int) $storeId;
        $result = StoreProduct::limit($limit)->offset($start)->where('count', '!=', 0)->where('store_id', $storeId)->get();
        $count = StoreProduct::where('count', '>', 0)->where('store_id', $storeId)->count();

        $table = [
            'draw'            => $page,
            'recordsTotal'    => $count,
            'recordsFiltered' => $count,
            'data'            => []
        ];

        /** @var StoreProduct $storeProduct */
        foreach ($result as $storeProduct) {
            $table['data'][] = [
                'id'    => $storeProduct->product->id,
                'name'  => $storeProduct->product->name,
                'count' => $storeProduct->count,
            ];
        }

        return $table;
    }

    /**
     * Получить список растрат магазина
     *
     * @param int $storeId
     * @param int $page какую страницу запросили
     * @param int $start
     * @param int $limit сколько выводится на странице
     * @return array
     */
    public function getStoreChargesList($storeId, $page = 1, $start = 0, $limit = 10)
    {
        $storeId = (int) $storeId;
        $result = StoreCharges::limit($limit)->offset($start)->where('store_id', $storeId)->get();
        $count = StoreCharges::where('store_id', $storeId)->count();

        $table = [
            'draw'            => $page,
            'recordsTotal'    => $count,
            'recordsFiltered' => $count,
            'data'            => []
        ];

        /** @var StoreCharges $charge */
        foreach ($result as $charge) {
            $table['data'][] = [
                'type'        => $charge->type->name,
                'description' => $charge->description,
                'cost'        => $charge->cost . ' руб.',
                'date'        => $charge->created_at->toDateTimeString()
            ];
        }

        return $table;
    }


    /**
     * Получить список цен на товары
     *
     * @param int $storeId
     * @return StoreProductPrice[]
     */
    public function getListProductPrice($storeId)
    {
        $result = StoreProductPrice::where(['store_id' => $storeId])->get();

        return $result;
    }

    /**
     * Добавить новый ценник на товар для магазина
     *
     * @param int $storeId
     * @param array $products
     * @return bool
     */
    public function addOrUpdateProductPrice($storeId, array $products)
    {
        $this->log(__METHOD__, func_get_args());

        $result = true;
        $old = StoreProductPrice::where([
            'store_id' => $storeId
        ]);
        $old->delete();

        foreach($products as $product) {
            $productPrice = new StoreProductPrice();

            $productPrice->product_id = $product['product_id'];
            $productPrice->price = $product['price'];
            $productPrice->store_id = $storeId;

            $saved = $productPrice->save();
            if ($result) {
                $result = $saved;
            }
        }

        return $result;
    }

    /**
     * Получить зафиксированные цены для конкретного магазина
     *
     * @param int $storeId
     * @param array $orderProductsId
     * @return array
     */
    public function getStorePriceProducts($storeId, array $orderProductsId = [])
    {
        $result = [];

        $query = StoreProductPrice::where(['store_id' => $storeId]);
        if ($orderProductsId) {
            $query = $query->whereIn('product_id', $orderProductsId);
        }

        $productPrice = $query->get();

        /** @var StoreProductPrice $product */
        foreach ($productPrice as $product) {
            $result[$product->product_id] = $product->price;
        }

        return $result;
    }

    /**
     * Зафиксировать новую растрату магазина
     *
     * @param int $storeId
     * @param int $chargeTypeId
     * @param float $cost
     * @param string $description
     *
     * @return bool
     */
    public function newChargesStore($storeId, $chargeTypeId, $cost, $description)
    {
        $this->log(__METHOD__, func_get_args());
        /** @var Store $store */
        $store = Store::findOrNew($storeId);

        if (!\Auth::user()->is('admin|moderator') && $store->user_id != \Auth::user()->id) {
            return false;
        }

        $charge = new StoreCharges();
        $charge->charge_type_id = $chargeTypeId;
        $charge->description = $description;
        $charge->store_id = $storeId;
        $charge->cost = $cost;

        $result = $charge->save();

        if ($result) {
            /** @var Store $store */
            $store = Store::findOrNew($storeId);
            $store->balance = $store->balance - $cost;
            $result = $store->save();
        }

        return $result;
    }
}