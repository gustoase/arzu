<?php

namespace App\Services;

use App\Stock;
use App\StockProduct;
use Log;

/**
 * Сервис отвечающий за работу со складом
 *
 * Class StockService
 * @package App\Services
 */
class StockService extends BaseService
{

    /**
     * @return Stock[]
     */
    public function getListStocks()
    {
        return Stock::all();
    }

    /**
     * @param $id
     *
     * @return Stock
     */
    public function getStock($id)
    {
        return Stock::findOrNew($id);
    }

    /**
     * Получить товары которые на складе
     *
     * Подготовленный ответ для таблицы
     *
     * @param $stockId
     * @param int $page какую страницу запросили
     * @param int $start
     * @param int $limit сколько выводится на странице
     * @param string $search
     * @return array
     */
    public function getStockProductList($stockId, $page = 1, $start = 0, $limit = 10, $search = null)
    {
        $query = StockProduct::select('stock_product.*')
            ->limit($limit)->offset($start);

        if (!is_null($search)) {
            $query->join('product', 'stock_product.product_id', '=', 'product.id');
            $query->where('stock_product.product_id', 'LIKE', "%$search%")
                  ->orWhere('product.name', 'LIKE', "%$search%");
        }

        $query->where(['stock_id' => $stockId]);

        $stockProducts = $query->get();

        $count = Stock::find($stockId)->stockProducts()->count();

        $table = [
            'draw'            => $page,
            'recordsTotal'    => $count,
            'recordsFiltered' => $count,
            'data'            => []
        ];

        $isAdmin = \Auth::user()->is('admin|moderator');

        /** @var StockProduct $product */
        foreach ($stockProducts as $stockProduct) {
            if (is_null($stockProduct->product)) {
                continue;
            }

            $table['data'][] = [
                'id'          => $stockProduct->product->id,
                'name'        => $stockProduct->product->name,
                'stock_name'  => $stockProduct->stock->name,
                'box_count'   => $stockProduct->boxCount(),
                'price'       => $isAdmin ? $stockProduct->price : '-',
                'in_box'      => $stockProduct->product->in_box,
                'price_box'   => $isAdmin ? $stockProduct->boxPrice() : '-',
                'count'       => $stockProduct->count,
                'supply_date' => $stockProduct->supply->created_at->toDateTimeString(),
            ];
        }

        return $table;
    }


    /**
     * @param int $stockId
     * @param array $fields ['name' => 'new value']
     * @return bool|int
     */
    public function updateStock($stockId, array $fields)
    {
        $this->log(__METHOD__, func_get_args());

        $stock = Stock::find($stockId);
        $result = $stock->update($fields);

        return $result;
    }

    /**
     * Добавить новый склад в систему
     *
     * @param string $name
     * @param string $address
     * @param string $phone
     * @param string $description
     *
     * @return Stock|bool
     */
    public function createStock($name, $address, $phone, $description)
    {
        $this->log(__METHOD__, func_get_args());

        $stock = new Stock();
        $stock->name = $name;
        $stock->address = $address;
        $stock->phone = $phone;
        $stock->description = $description;

        $result = $stock->save();

        if ($result) {
            return $stock;
        } else {
            return false;
        }
    }


    /**
     * Поместить поставленный товар на склад, а если есть то обновить кол-во и цену
     *
     * @param int $stockId
     * @param int $productId
     * @param int $count
     * @param float $price
     * @param int $supplyId
     * @return bool
     */
    public function addProductToStock($stockId, $productId, $count, $price, $supplyId)
    {
        $this->log(__METHOD__, func_get_args());

        $stockProduct = StockProduct::firstOrNew([
            'product_id' => $productId,
            'stock_id'   => $stockId
        ]);

        $currentCount = 0;
        if ($stockProduct->id) {
            $currentCount = $stockProduct->count;
        } else {
            $stockProduct = new StockProduct();
        }

        $stockProduct->product_id = $productId;
        $stockProduct->count = $currentCount + $count;
        $stockProduct->price = $price;
        $stockProduct->stock_id = $stockId;
        $stockProduct->last_supply_id = $supplyId;

        $result = $stockProduct->save();

        if (!$result) {
            Log::error('Не удалось сохранить товар на склад', ['product' => $stockProduct]);
        }

        return $result;
    }

    /**
     * Уменьшить товара на складе
     *
     * @param int $stockId
     * @param int $productId
     * @param int $minusCount
     * @return bool
     */
    public function reduceTheAmountProductFromStock($stockId, $productId, $minusCount)
    {
        $this->log(__METHOD__, func_get_args());

        /** @var StockProduct $stockProduct */
        $stockProduct = StockProduct::where([
            'stock_id'   => $stockId,
            'product_id' => $productId
        ])->first();

        $stockProduct->count = $stockProduct->count - (int) $minusCount;

        $result = $stockProduct->save();

        return $result;
    }
}