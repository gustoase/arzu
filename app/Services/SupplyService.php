<?php

namespace App\Services;

use App\Supply;
use App\SupplyProduct;
use Log;

/**
 * Сервис отвечающий за работу с поставками
 *
 * Class SupplyService
 * @package App\Services
 */
class SupplyService extends BaseService
{

    /**
     * @var StockService
     */
    protected $stockService;

    /**
     * @param StockService $stockService
     */
    public function __construct(StockService $stockService) {
        $this->stockService = $stockService;
    }

    /**
     * Подготовленный список поставок для вывода в таблице
     *
     * @param int $page
     * @param int $start
     * @param int $limit
     * @param null $search что пользователь ищет
     * @return array
     */
    public function getAllSupply($page = 1, $start = 0, $limit = 10, $search = null) {

        $query = Supply::limit($limit)->offset($start)->orderBy('created_at', 'desc');

        if (!is_null($search)) {
            $query->where('provider', 'LIKE', "%$search%")
                ->orWhere('id', 'LIKE', "%$search%");
        }

        $supplyList = $query->get();

        $count = Supply::all()->count();

        $table = [
            'draw'            => $page,
            'recordsTotal'    => $count,
            'recordsFiltered' => $count,
            'data'            => []
        ];

        /** @var Supply $supply */
        foreach ($supplyList as $supply) {

            // список товаров в поставке
            $products = [];

            /** @var SupplyProduct $supplyProduct */
            foreach ($supply->supplyProducts as $supplyProduct) {
                $products[] = [
                    'id'        => $supplyProduct->product->id,
                    'name'      => $supplyProduct->product->name,
                    'count'     => $supplyProduct->count,
                    'price'     => $supplyProduct->price,
                    'price_sum' => $supplyProduct->price * $supplyProduct->count,
                ];
            }

            $table['data'][] = [
                'id'         => $supply->id,
                'provider'   => $supply->provider,
                'stock_id'   => $supply->stock->id,
                'stock_name' => $supply->stock->name,
                'date'       => $supply->created_at->toDateTimeString(),
                'products'   => $products
            ];
        }

        return $table;
    }

    /**
     * Добавить новую поставку
     *
     * @param int $stockId
     * @param string $provider
     * @param SupplyProduct[] $products
     *
     * @return Supply|bool
     */
    public function createSupply($stockId, $provider, array $products)
    {
        $this->log(__METHOD__, func_get_args());

        $supply = new Supply();
        $supply->stock_id = $stockId;
        $supply->provider = $provider;

        $result = $supply->save();

        if ($result) {
            foreach ($products as $product) {
                $product->supply_id = $supply->id;
                $saved = $product->save();

                if ($saved) {
                    $this->stockService->addProductToStock(
                        $stockId,
                        $product->product_id,
                        $product->count,
                        $product->price,
                        $supply->id
                    );
                } else {
                    $result = false;

                    \Log::error('Не удалось сохранить товар в поставке', ['product' => $product]);
                }
            }
        }

        if ($result) {
            return $supply;
        } else {
            return false;
        }

    }
}