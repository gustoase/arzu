<?php

namespace App\Services;

use App\Order;
use \Mail;

/**
 * Класс отправки писем
 *
 * Class EmailService
 * @package app\Services
 */
class EmailService extends BaseService
{
    /**
     * Почта админа
     *
     * @var string
     */
    protected $adminEmail;

    public function __construct()
    {
        $config = config('mail');

        $this->adminEmail = $config['from']['address'];
    }

    /**
     * Оповестить о новом заказе админу
     *
     * @param int $orderId
     */
    public function newOrder($orderId)
    {
        /** @var Order $order */
        $order  = Order::findOrNew($orderId);
        $store  = $order->store;

        Mail::send('email.order_new', ['store' => $store, 'products' => $order->orderProducts, 'orderId' => $order->id],
            function ($m) use ($store) {
                $m->to($this->adminEmail)->subject('Новый заказ от - ' . $store->name);
            });
    }

    /**
     * Оповестить о том что заказ собран админом
     *
     * @param int $orderId
     */
    public function orderShipped($orderId)
    {
        /** @var Order $order */
        $order  = Order::findOrNew($orderId);
        $store  = $order->store;

        Mail::send('email.order_shipped', ['store' => $store, 'products' => $order->orderProducts, 'orderId' => $order->id],
            function ($m) use ($orderId, $store) {
                $m->to($store->user->email)->subject('Заказ №'.$orderId.' собран');
            });
    }

    /**
     * Оповестить о том что заказ был реализован магазином
     *
     * @param int $orderId
     */
    public function orderCompleted($orderId)
    {
        /** @var Order $order */
        $order  = Order::findOrNew($orderId);
        $store  = $order->store;

        Mail::send('email.order_completed', ['store' => $store, 'products' => $order->orderProducts, 'orderId' => $order->id],
            function ($m) use ($orderId, $store) {
                $m->to($this->adminEmail)->subject('Заказ №'.$orderId.' реализован');
            });
    }
}