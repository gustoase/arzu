<?php

namespace App\Services;

use App\Order;
use App\OrderProduct;
use App\Stock;
use App\Store;
use App\StoreProduct;
use App\User;

/**
 * Сервис отвечающий за работу с заказами
 *
 * Class OrderService
 * @package App\Services
 */
class OrderService extends BaseService
{
    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * @param EmailService $emailService
     */
    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }


    /**
     * Получить список заказов у магазина
     *
     * @param int $storeId
     * @param string $filterList not_completed || completed
     * @param int $page
     * @param int $start
     * @param int $limit
     * @return \App\Order[]
     */
    public function getListOrder($storeId, $filterList, $page = 1, $start = 0, $limit = 10)
    {
        if ($filterList == 'not_completed') {
            $condition = '!=';
        } else {
            $condition = '=';
        }

        $orderListQuery = Order::limit($limit)->offset($start)
                            ->where('status', $condition, 'completed')->orderBy('id', 'desc');

        $countQuery = Order::where('status', $condition, 'completed');

        if (!is_null($storeId)) {
            $orderListQuery->where(['store_id' => $storeId]);
            $count = $countQuery->where(['store_id' => $storeId])->count();
        } else {
            $count = $countQuery->count();
        }

        $orderList = $orderListQuery->get();

        $table = [
            'draw'            => $page,
            'recordsTotal'    => $count,
            'recordsFiltered' => $count,
            'data'            => []
        ];

        /** @var Order $order */
        foreach ($orderList as $order) {
            $table['data'][] = [
                'id'          => $order->id,
                'status'      => Order::$dictionary_status[$order->status],
                'status_en'   => $order->status,
                'description' => $order->description,
                'date'        => $order->created_at->toDateTimeString()
            ];
        }

        return $table;
    }


    /**
     * Получить список товаров в заказе
     *
     * @param int $orderId
     * @return OrderProduct[]
     */
    public function getOrderProducts($orderId)
    {
        $orders = OrderProduct::where(['order_id' => $orderId])->get();

        return $orders;
    }


    /**
     * Обновить информацию о товаре в заказе, например кол-во или цены
     *
     * @param int $orderProductId
     * @param array $fields ['name' => 'new value']
     * @return bool|int
     */
    public function updateOrderProduct($orderProductId, array $fields)
    {
        $this->log(__METHOD__, func_get_args());
        $orderProduct = OrderProduct::find($orderProductId);
        $result = $orderProduct->update($fields);

        return $result;
    }

    /**
     * Обновить заказ в целом, перевести в другой статус и др. бизнес логика, перевода продуктов на склад и т.п
     *
     * @param int $orderId
     * @param User $user
     * @param array $fields ['name' => 'new value']
     * @return bool|int
     */
    public function updateOrder($orderId, User $user, array $fields)
    {
        $this->log(__METHOD__, func_get_args());
        /** @var Order $order */
        $order = Order::findOrNew($orderId);
        $isOwner = $order->store->user_id === $user->id;

        if (!$user->is('admin|moderator') && !$isOwner) {
            return false;
        }

        // в случае если хотят обновить статус заказа на "завершенный"
        // но статус текущего заказа только "созданный" то нельзя,
        // нужно чтобы заказ был уже собран и отправлен
        if (
            isset($fields['status']) &&
            $fields['status'] == Order::STATUS_COMPLETED &&
            $order->status != Order::STATUS_SHIPPED
        ) {
            return false;
        }

        // в момент отправки товара со склада, вычесть со склада товар
        if (isset($fields['status']) && $fields['status'] == Order::STATUS_SHIPPED) {
            $this->updateStock($order);
            $this->emailService->orderShipped($order->id);
        }

        $result = $order->update($fields);

        if ($result && $fields['status'] == Order::STATUS_COMPLETED) {
            $result = $this->completedOrder($orderId);
            $this->emailService->orderCompleted($order->id);
        }

        return $result;
    }

    /**
     * Обновляем склад, уменьшаем на складе товар при отправке заказа в статус Order::STATUS_SHIPPED
     * @param Order $order
     */
    protected function updateStock(Order $order)
    {
        /** @var StockService $stockService */
        $stockService = app('App\Services\StockService');

        if (!$order->orderProducts) {
            return;
        }

        foreach($order->orderProducts as $product) {
            $stockService->reduceTheAmountProductFromStock(
                $product->stock_id,
                $product->product_id,
                $product->count
            );
        }
    }

    /**
     * Получить кол-во не завершенных заказов
     *
     * @param int $storeId
     * @return int
     */
    public function getCountNotCompleted($storeId)
    {
        $count = $this->getCountByConditionStatus($storeId, '!=');

        return $count;
    }

    /**
     * Получить кол-во завершенных заказов
     *
     * @param int $storeId
     * @return int
     */
    public function getCountCompleted($storeId)
    {
        $count = $this->getCountByConditionStatus($storeId, '=');

        return $count;
    }

    /**
     * Получить кол-во новых заказов
     *
     * @param int $storeId
     * @return int
     */
    public function getCountNew($storeId)
    {
        $count = $this->getCountByConditionStatus($storeId, '=', Order::STATUS_NEW);

        return $count;
    }

    /**
     * Получить кол-во заказов готовых или нет
     *
     * @param int $storeId
     * @param string $condition
     * @param string $status
     * @return int
     */
    protected function getCountByConditionStatus($storeId, $condition = '=', $status = Order::STATUS_COMPLETED)
    {
        $countQuery = Order::where('status', $condition, $status);

        if (!is_null($storeId)) {
            $countQuery->where('store_id', '=', $storeId);
        }

        $count = $countQuery->count();

        return $count;
    }

    /**
     * Обновить заказ в целом,
     * все товары которые не были проданы поместить на склад магазина и обновить баланс
     *
     * @param int $orderId
     *
     * @return bool
     */
    protected function completedOrder($orderId)
    {
        /** @var OrderProduct[] $orderProducts */
        $orderProducts = OrderProduct::where(['order_id' => $orderId])->get();

        $this->log(__METHOD__, ['orderProducts' => $orderProducts, 'orderId' => $orderId]);

        // товары которые уйдут на склад магазина
        $productsToStockStore = [];
        // выручка по заказу
        $balanceOrder = 0.0;

        /** @var Order $order */
        $order = Order::findOrNew($orderId);
        /** @var Store $store */
        $store = $order->store;

        foreach ($orderProducts as $orderProduct) {
            $balanceOrder += $orderProduct->price_out * $orderProduct->count_out;


            // в случае если товара продали меньше чем положено, то отложить на склад или наоборот вычесть
            if ($orderProduct->count_out < $orderProduct->count) {
                $minusStore = false;
            } elseif ($orderProduct->count_out > $orderProduct->count) {
                $minusStore = true;
            } else {
                continue;
            }

            /** @var StoreProduct $storeProduct */
            $storeProduct = StoreProduct::where([
                'store_id'   => $store->id,
                'product_id' => $orderProduct->product_id
            ])->first();

            // если на складе уже есть такой товар, то обновляем его кол-во
            if ($storeProduct) {
                if ($minusStore) {
                    // в случае когда продали больше чем положено, то вычитает со склада магазина,
                    // если какой-то левак то фиксируется отрицательное значение
                    $storeProduct->count = $storeProduct->count - ($orderProduct->count_out - $orderProduct->count);
                } else {
                    $storeProduct->count = $storeProduct->count + ($orderProduct->count - $orderProduct->count_out);
                }

                $storeProduct->save();
                continue;
            }

            // если на складе такого товара не было то добавляем
            $storeProduct = new StoreProduct();
            $storeProduct->count      = $orderProduct->count - $orderProduct->count_out;
            $storeProduct->product_id = $orderProduct->product_id;
            $storeProduct->store_id   = $store->id;
            $storeProduct->save();
        }

        $this->log(__METHOD__, ['startStoreBalance' => $store->balance, 'balanceOrder' => $balanceOrder]);

        $storeBalance = $store->balance + $balanceOrder;

        $result = $store->update([
            'balance' => $storeBalance
        ]);

        return $result;
    }

    /**
     * Создать новый заказ
     *
     * @param int $storeId
     * @param string $description
     * @param OrderProduct[] $products
     * @return Order|bool
     */
    public function createOrder($storeId, $description, array $products)
    {
        $this->log(__METHOD__, func_get_args());

        $order = new Order();
        $order->store_id = $storeId;
        $order->description = $description;

        $stock = Stock::take(1)->first();

        $result = $order->save();

        if ($result) {
            foreach ($products as $product) {
                $product->order_id = $order->id;
                $product->stock_id = $stock->id;
                $saved = $product->save();


                if (!$saved) {
                    $result = false;
                    \Log::error('Не удалось сохранить заказ', ['product' => $product]);
                }
            }
        }

        if ($result) {
            $this->emailService->newOrder($order->id);
            return $order;
        } else {
            return false;
        }

    }

    /**
     * Редактировать заказ
     *
     * @param int $orderId
     * @param int $storeId
     * @param string $description
     * @param OrderProduct[] $products
     * @return Order|bool
     */
    public function editOrder($orderId, $storeId, $description, array $products)
    {
        $this->log(__METHOD__, func_get_args());

        /** @var Order $order */
        $order = Order::findOrNew($orderId);
        $order->store_id = $storeId;
        $order->description = $description;

        $result = $order->save();

        OrderProduct::where(['order_id' => $orderId])->delete();

        if ($result) {
            foreach ($products as $product) {
                $product->order_id = $order->id;
                $saved = $product->save();

                if (!$saved) {
                    $result = false;
                    \Log::error('Не удалось сохранить заказ', ['product' => $product]);
                }
            }
        }

        if ($result) {
            return $order;
        } else {
            return false;
        }

    }
}