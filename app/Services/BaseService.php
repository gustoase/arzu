<?php

namespace App\Services;

/**
 * Базовый сервис с разными полезными методами
 *
 * Class BaseService
 * @package app\Services
 */
class BaseService
{
    /**
     * Логируем все вызовы сервиса
     *
     * @param string $msg
     * @param array $args
     * @return $this
     */
    public function log($msg, array $args)
    {
        $args['user_id'] = \Auth::user()->id;

        \Log::info($msg, $args);

        return $this;
    }
}