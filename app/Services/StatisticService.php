<?php


namespace App\Services;
use App\Order;
use App\OrderProduct;
use App\Store;
use App\StoreCharges;

/**
 * Class StatisticService
 * @package app\Services
 */
class StatisticService extends BaseService
{
    /**
     * Получить статистику по магазинам
     *
     * @param string $dateStart
     * @param string $dateEnd
     * @return array
     */
    public function getStatisticOrdersStore($dateStart, $dateEnd)
    {
        $stores = Store::all();

        $storesStatistics = [];

        /** @var Store $store */
        foreach($stores as $store) {
            $info = [
                'store_id'            => $store->id,
                'store_name'          => $store->name,
                'date_start'          => $dateStart,
                'date_end'            => $dateEnd,
                'count_orders'        => 0,
                'sum_price_in_order'  => 0.0, // на какую сумму был отдан товар
                'sum_price_out_order' => 0.0, // на какую сумму был продан товар магазином
                'sum_charges_cost'    => 0.0, // растраты магазина
                'sum_price_profit'    => 0.0, // итоговая выручка для магазина
                'balance'             => $store->balance, // текущий баланс магазина
                'statistic_with_date_for_chart' => [] // сгруппированные выручки по дням
            ];

            // получаем все заказы из каждого магазина за промежуток времени
            $orderList = Order::where(['store_id' => $store->id, 'status' => Order::STATUS_COMPLETED])
                       ->whereBetween('created_at', [$dateStart, $dateEnd])->get();

            $sumPriceInOrder  = 0.0;
            $sumPriceOutOrder = 0.0;
            // фиксируем профит магазина по датам
            $statisticWithDateForChart = [];

            /** @var Order $order */
            foreach ($orderList as $order) {
                $info['count_orders']++;

                $currentOrderSumPriceIn  = 0.0; // сумма по конкретному заказу
                $currentOrderSumPriceOut = 0.0;
                if (!isset($statisticWithDateForChart[$order->created_at->toDateString()])) {
                    $statisticWithDateForChart[$order->created_at->toDateString()] = 0.0;
                }

                /** @var OrderProduct $orderProduct */
                foreach ($order->orderProducts as $orderProduct) {
                    $currentOrderSumPriceIn  += $orderProduct->price_in  * $orderProduct->count;
                    $currentOrderSumPriceOut += $orderProduct->price_out * $orderProduct->count_out;
                }

                // фиксируем прибыль конкретно по заказам и месяцам
                $statisticWithDateForChart[$order->created_at->toDateString()] += $currentOrderSumPriceOut - $currentOrderSumPriceIn;

                // плюсуем суммы к общей статистике магазина
                $sumPriceInOrder  += $currentOrderSumPriceIn;
                $sumPriceOutOrder += $currentOrderSumPriceOut;
            }


            // получаем все растраты каждого магазина за промежуток времени
            $chargesList = StoreCharges::where(['store_id' => $store->id])
                                ->whereBetween('created_at', [$dateStart, $dateEnd])->get();

            $sumChargesCost = 0.0;
            /** @var StoreCharges $charge */
            foreach ($chargesList as $charge) {
                $sumChargesCost += $charge->cost;
            }

            // высчитываем выручку
            $sumPriceProfit = ($sumPriceOutOrder - $sumPriceInOrder) - $sumChargesCost;

            $info['sum_price_in_order']  = $sumPriceInOrder;
            $info['sum_price_out_order'] = $sumPriceOutOrder;
            $info['sum_charges_cost']    = $sumChargesCost;
            $info['sum_price_profit']    = $sumPriceProfit;
            $info['statistic_with_date_for_chart'] = $statisticWithDateForChart;

            $storesStatistics[] = $info;
        }

        return $storesStatistics;
    }
}