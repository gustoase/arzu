<?php

namespace App\Services;


use App\Product;
use Illuminate\Support\Facades\Auth;

/**
 * Сервис отвечающий за работу с продуктами
 *
 * Class ProductService
 * @package App\Services
 */
class ProductService extends BaseService
{

    /**
     * Подготовленный ответ для таблицы
     * @param int $page какую страницу запросили
     * @param int $start
     * @param int $limit сколько выводится на странице
     * @return array
     */
    public function getAllProducts($page = 1, $start = 0, $limit = 10)
    {
        $result = Product::limit($limit)->offset($start)->where('is_deleted', 0)->get();
        $count = Product::where('is_deleted', 0)->count();

        $table = [
            'draw' => $page,
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => []
        ];

        /** @var Product $product */
        foreach($result as $product) {
            $table['data'][] = [
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description,
                'price' => $product->price,
                'in_box' => $product->in_box,
                'price_box' => $product->priceBox(),
            ];
        }

        return $table;
    }

    /**
     * @param int $productId
     * @return Product
     */
    public function getProduct($productId)
    {
        $result = Product::findOrNew($productId);

        return $result;
    }

    /**
     * @param int $productId
     * @param array $fields ['name' => 'new value']
     * @return bool|int
     */
    public function updateProduct($productId, array $fields)
    {
        $this->log(__METHOD__, func_get_args());

        $product = Product::find($productId);
        $result = $product->update($fields);

        return $result;
    }

    /**
     * @param int $productId
     * @return bool|int
     */
    public function removeProduct($productId)
    {
        $this->log(__METHOD__, func_get_args());
        /** @var Product $product */
        $product = Product::find($productId);
        $product->is_deleted = 1;
        $result = $product->save();

        return $result;
    }

    /**
     * Добавить новый товар в систему
     *
     * @param string $name
     * @param string $price
     * @param string $inBox
     * @param string $description
     * @return Product|bool
     */
    public function createProduct($name, $price, $inBox, $description)
    {
        $this->log(__METHOD__, func_get_args());
        $product = new Product();
        $product->name = $name;
        $product->price = $price;
        $product->in_box = $inBox;
        $product->description = $description;

        $result = $product->save();

        if ($result) {
            return $product;
        } else {
            return false;
        }
    }
}