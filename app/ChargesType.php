<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Тип расходов магазина, например зарплата и т.п
 *
 * Class ChargesType
 * @package App
 *
 * @property int $id
 * @property string $name
 */
class ChargesType extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'charges_type';
}
