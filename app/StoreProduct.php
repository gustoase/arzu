<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Товары которые есть в магазине на данный момент
 *
 * Class StoreProduct
 * @package App
 *
 * @property int $id
 * @property int $store_id
 * @property int $product_id
 * @property int $count
 * @property string $created_at
 * @property Product $product
 * @property Store $store
 */
class StoreProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_product';

    // Получить связанный продукт
    public function product() {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

    public function store() {
        return $this->belongsTo('App\Store');
    }
}
