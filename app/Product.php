<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Продукт/Товар
 *
 * Class Product
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $in_box
 * @property float $price
 * @property string $created_at
 * @property int is_deleted
 * @property Supply[] supplies
 * @property Stock[] stocks
 */
class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';

    protected $fillable = ['name', 'product_id', 'price', 'count', 'last_supply_id'];

    public function stocks()
    {
        return $this->hasManyThrough('App\Stock', 'App\StockProduct');
    }

    public function supplies()
    {
        return $this->hasManyThrough('App\Supply', 'App\SupplyProduct');
    }

    /**
     * Цена за упаковку
     * @return float
     */
    public function priceBox() {
        return $this->price * $this->in_box;
    }
}
