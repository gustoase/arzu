<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Магазин
 *
 * Class Store
 * @package App
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $description
 * @property float $balance
 * @property string $created_at
 * @property User $user
 * @property StoreProduct[] $storeProducts
 */
class Store extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store';

    protected $fillable = ['name', 'description', 'balance', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function storeProducts() {
        return $this->hasMany('App\StoreProduct');
    }
}
