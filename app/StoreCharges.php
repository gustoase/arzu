<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Расходы магазина
 *
 * Class StoreCharges
 * @package App
 *
 * @property int $id
 * @property float $cost стоимость расхода
 * @property int $charge_type_id
 * @property int $store_id
 * @property string $description
 * @property string $created_at
 * @property ChargesType $type
 */
class StoreCharges extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'store_charges';

    protected $fillable = ['cost', 'charge_type_id', 'store_id', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type() {
        return $this->hasOne('App\ChargesType', 'id', 'charge_type_id');
    }
}
