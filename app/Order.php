<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Заказ от магазина на склад
 *
 * Class Order
 * @package App
 *
 *
 * @property int $id
 * @property int $store_id
 * @property string $status 'new','shipped','completed','error'
 * @property string $description
 * @property Carbon $created_at
 * @property OrderProduct[] $orderProducts
 * @property Store $store
 */
class Order extends Model
{
    // статусы заказа
    const STATUS_NEW = 'new';
    const STATUS_SHIPPED = 'shipped';
    const STATUS_COMPLETED = 'completed';
    const STATUS_ERROR = 'error';

    /**
     * @var array
     */
    public static $dictionary_status = [
        self::STATUS_NEW       => 'новый',
        self::STATUS_SHIPPED   => 'отправлен',
        self::STATUS_COMPLETED => 'выполнен',
        self::STATUS_ERROR     => 'ошибка'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order';

    protected $fillable = ['store_id', 'description', 'status'];

    public function orderProducts() {
        return $this->hasMany('App\OrderProduct');
    }

    public function store() {
        return $this->hasOne('App\Store', 'id', 'store_id');
    }
}
