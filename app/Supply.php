<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Поставка на склад
 *
 * Class Supply
 * @package App
 *
 * @property int $id
 * @property int $stock_id
 * @property string $provider поставщик
 * @property string $created_at
 * @property SupplyProduct[] $supplyProducts
 * @property Stock $stock
 */
class Supply extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'supply';

    public function supplyProducts() {
        return $this->hasMany('App\SupplyProduct');
    }

    // Получить связанный склад
    public function stock() {
        return $this->hasOne('App\Stock', 'id', 'stock_id');
    }
}
