<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class StatisticServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Services\StatisticService', 'App\Services\StatisticService');
    }
}
