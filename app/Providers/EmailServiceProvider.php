<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class EmailServiceProvider
 * @package app\Providers
 */
class EmailServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Services\EmailService', 'App\Services\EmailService');
    }
}