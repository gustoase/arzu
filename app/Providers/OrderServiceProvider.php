<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class OrderServiceProvider
 * @package app\Providers
 */
class OrderServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Services\OrderService', 'App\Services\OrderService');
    }
}