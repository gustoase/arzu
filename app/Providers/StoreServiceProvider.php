<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class StoreServiceProvider
 * @package app\Providers
 */
class StoreServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Services\StoreService', 'App\Services\StoreService');
    }
}