<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class SupplyServiceProvider
 * @package app\Providers
 */
class SupplyServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Services\SupplyService', 'App\Services\SupplyService');
    }
}