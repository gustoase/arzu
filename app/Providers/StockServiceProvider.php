<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class StockServiceProvider
 * @package app\Providers
 */
class StockServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Services\StockService', 'App\Services\StockService');
    }
}