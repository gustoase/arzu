<?php

namespace app\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class ProductServiceProvider
 * @package app\Providers
 */
class ProductServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Services\ProductService', 'App\Services\ProductService');
    }
}