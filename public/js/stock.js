$(function () {

    // возможность редактировать склад
    if ($('#stock-info a').length) {
        $('#stock-info a').editable({
            url: $('#stock-info').data().url
        });
    }

    // отображается таблица продуктов
    $('#stock-product-list-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "bSort": false,
        "language" : $.datatableLang,
        "ajax": {
            "url": location.href,
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "stock_name" },
            { "data": "count" },
            { "data": "box_count" },
            { "data": "in_box" },
            { "data": "price" },
            { "data": "price_box" },
            { "data": "supply_date" }
        ],
        "columnDefs": [{
                "render": function ( data, type, row ) {
                    return '<a href="/product/'+data+'">'+data+'</a>';
                },
                "targets": 0 // преобразуем ID товара в ссылку
            }]
    } );

    // создание нового склада
    if ($('#new-stock a').length) {
        var newStock = $('#new-stock a');

        newStock.editable();

        $('#save-btn').click(function() {
            $("#overlay").fakeLoader();

            newStock.editable('submit', {
                type: 'post',
                url: '/stock/new',
                ajaxOptions: {
                    dataType: 'json' //assuming json response
                },
                success: function(data) {
                    location.replace('/stock/'+data.id+'/products');
                },
                error: function(errors) {
                    var msg = '';
                    if(errors && errors.responseText) { //ajax error, errors = xhr object
                        msg = errors.responseText;
                    }
                    $('#msg').html(msg).removeClass('hide');
                    $("#overlay").fadeOut();
                }
            });
        });
    }

});