$(function() {

    // редактирование заказа
    if ($('#edit-order').length || $('#new-order').length) {


        if ($('#new-order').length) {
            var url = '/order/new';
            var productsSelector = '#new-order .product-item';
        } else {
            var url = location.href;
            var productsSelector = '#edit-order .product-item';
        }

        $('#description').editable();

        $('#store-id').editable({
            source: $(this).data().source,
            select2: {
                width: 200,
                placeholder: 'От какого магазина?',
                allowClear: true
            }
        });

        initEditableProductFields();

        // при отправке формы собираем введенные данные
        $('#save-btn').click(function() {
            $("#overlay").fakeLoader();

            // от какого магазина поступил заказ
            var storeField =  $('#store-id').editable('getValue');
            var descriptionField =  $('#description').editable('getValue');

            if (typeof storeField['store_id'] == 'undefined') {
                alert('Нужно выбрать магазин!');

                $("#overlay").fadeOut();
                return;
            }

            var form  = {
                storeId: storeField.store_id,
                description: descriptionField.description,
                products: []
            };


            // пробигаемся по всем строкам добавленных продуктов, и собираем что ввели
            $(productsSelector).each(function() {
                var val = $(this).find('a').editable('getValue');

                form.products.push(val);
            });

            if (form.products.length == 0) {
                alert('Нужно выбрать товары');

                $("#overlay").fadeOut();
                return;
            }

            $.ajax({
                method: "POST",
                url: url,
                data: form,
                dataType: 'json'
            })
            .done(function( msg ) {
                location.reload();
            })
            .fail(function(errors) {
                var msg = '';
                if(errors && errors.responseText) { //ajax error, errors = xhr object
                    msg = errors.responseText;
                }
                $('#msg').html(msg).removeClass('hide');


                $("#overlay").fadeOut();
            });
        });

        initProductsFields();
    }


    // добавляем динамически строку продукта в таблицу
    function initProductsFields() {
        $('.add-product').click(function(e){
            e.preventDefault();
            var row = $('<tr class="product-item">' +
            '<td><a href="#" class="select2-product" data-type="select2" data-name="product_id" data-original-title="Товар">не указано</a></td>' +
            '<td><a href="#" data-type="text" data-name="count" data-original-title="Кол-во">0</a></td>' +
            '<td><button type="button" class="btn btn-default btn-xs remove-field"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> удалить</button></td>' +
            '</tr>');

            $('#new-order, #edit-order tbody').append(row);

            row.find('a:not(.select2-product)').editable();

            row.find('.select2-product').editable({
                source: productsList, // глобальный объект с продуктами
                select2: {
                    width: 200,
                    placeholder: 'Товар',
                    allowClear: false
                }
            });
        });

        //удалить строку с продуктом
        $('#new-order, #edit-order').on("click",".product-item .remove-field", function(e){
            e.preventDefault(); $(this).parents('.product-item').remove();
        });
    }

    function initEditableProductFields() {
        $('.product-item').each(function() {

            $(this).find('a:not(.select2-product)').editable();

            $(this).find('.select2-product').editable({
                source: productsList, // глобальный объект с продуктами
                select2: {
                    width: 200,
                    placeholder: 'Товар',
                    allowClear: false
                }
            });
        });
    }

     // отобразить список заказов
    if ($('#order-list-table').length) {
        // отображается таблица заказов
        var tableSelector = $('#order-list-table');
        var table = tableSelector.DataTable( {
            "processing": true,
            "serverSide": true,
            "bFilter" : false,
            "bSort": false,
            "language" : $.datatableLang,
            "ajax": {
                "url": location.href,
                "type": "POST"
            },
            "columns": [
                { "data": "id" },
                { "data": "status" },
                { "data": "description" },
                { "data": "date" }
            ],
            "columnDefs": [{
                "render": function ( data, type, row ) {
                    switch (row['status_en']) {
                        case 'new':
                            var label = 'label-primary';
                            break;
                        case 'shipped':
                            var label = 'label-warning';
                            break;
                        case 'completed':
                            var label = 'label-success';
                            break;
                        case 'error':
                            var label = 'label-danger';
                            break;
                        default:
                            var label = 'label-default';
                    }

                    return '<span class="label '+label+'">'+data+'</span>';
                },
                "targets": 1
            }]
        });

        // при щелчке на строку с заказом, отобразить список товаров в нем
        table.on( 'draw', function () {
            tableSelector.find('tr').on('click', function() {
                var aPos  = tableSelector.dataTable().fnGetPosition(this);
                var aData = tableSelector.dataTable().fnGetData(aPos[0]);

                $("#overlay").fakeLoader();
                $.get('/order/products', { order_id: aData[aPos].id})
                    .done(function( data ) {
                        $("#overlay").fadeOut();

                        $('.order-products-modal .modal-content').html(data);
                        initUpdateOrderProduct(aData[aPos].id);
                        initTooltip();
                        initDropDown();
                        $('.order-products-modal').modal();
                    });
            });
        });


        // инитим редактирование товара
        function initUpdateOrderProduct(orderId) {
            $('.update-order-product').editable();
            $('.update-order-product-stock').editable({
                source: $(this).data().source,
                select2: {
                    width: 200,
                    placeholder: 'С какого склада?',
                    allowClear: true
                }
            });

            $('.to-shipped-order').click(function() {
                $("#overlay").fakeLoader();
                if (!confirm("Точно заказ готов к отправке?")) {
                    $("#overlay").fadeOut();
                    return;
                }

                $.ajax({
                    method: "PUT",
                    url: "/order",
                    data: { pk: orderId, name: 'status', value: 'shipped'}
                })
                    .done(function( msg ) {
                        $("#overlay").fadeOut();

                        $('.order-products-modal').modal('hide');
                        table.ajax.reload();
                    })
                    .fail(function(msg){
                        $("#overlay").fadeOut();

                        alert('Ошибка');
                    });
            });

            $('.close-order').click(function() {
                $("#overlay").fakeLoader();
                if (!confirm("Точно заказ готов?")) {
                    $("#overlay").fadeOut();
                    return;
                }

                $.ajax({
                    method: "PUT",
                    url: "/order",
                    data: { pk: orderId, name: 'status', value: 'completed'}
                })
                    .done(function( msg ) {
                        $("#overlay").fadeOut();

                        $('.order-products-modal').modal('hide');
                        table.ajax.reload();
                    })
                    .fail(function(msg){
                        $("#overlay").fadeOut();

                        alert('Ошибка');
                    });
            });
        }
    }

});