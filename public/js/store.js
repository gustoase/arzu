$(function () {

    // возможность редактировать магазин
    if ($('#store-info a').length) {
        var url = $('#store-info').data().url;
        $('#store-info a:not(#user_id)').editable({
            url: url
        });

        $('#user_id').editable({
            source: $(this).data().source,
            select2: {
                width: 200,
                placeholder: 'Кто будет управлять?',
                allowClear: true
            },
            url: url
        });
    }

    // отображается таблица заказов магазина
    var tableSelector = $('#store-order-list-table');
    var table = tableSelector.DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter" : false,
        "bSort": false,
        "language" : $.datatableLang,
        "ajax": {
            "url": location.href,
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "status" },
            { "data": "description" },
            { "data": "date" }
        ],
        "columnDefs": [{
                "render": function ( data, type, row ) {
                    switch (row['status_en']) {
                        case 'new':
                            var label = 'label-primary';
                            break;
                        case 'shipped':
                            var label = 'label-warning';
                            break;
                        case 'completed':
                            var label = 'label-success';
                            break;
                        case 'error':
                            var label = 'label-danger';
                            break;
                        default:
                            var label = 'label-default';
                    }

                    return '<span class="label '+label+'">'+data+'</span>';
                },
                "targets": 1
            }]
    });

    // при щелчке на строку с заказом, отобразить список товаров в нем
    table.on( 'draw', function () {
        tableSelector.find('tr').on('click', function() {
            var aPos  = tableSelector.dataTable().fnGetPosition(this);
            var aData = tableSelector.dataTable().fnGetData(aPos[0]);

            $("#overlay").fakeLoader();
            $.get('/order/products', { order_id: aData[aPos].id})
                .done(function( data ) {
                    $("#overlay").fadeOut();

                    $('.order-products-modal .modal-content').html(data);
                    initUpdateOrderProduct(aData[aPos].id);
                    initTooltip();
                    initDropDown();
                    $('.order-products-modal').modal();
                });
        });
    });

    // создание нового магазина
    if ($('#new-store a').length) {

        // делаем так потому что автоматически селект2 не цепляет данные, нужно инитить отдельно
        $('#new-store a:not(#user-id)').editable();

        $('#user-id').editable({
            source: $(this).data().source,
            select2: {
                width: 200,
                placeholder: 'Кто будет управлять?',
                allowClear: true
            }
        });

        // при отправке формы собираем введенные данные
        $('#save-btn').click(function() {
            $("#overlay").fakeLoader();

            $.ajax({
                method: "POST",
                url: "/store/new",
                data: $('#new-store a').editable('getValue'),
                dataType: 'json'
            })
            .done(function( msg ) {
                location.reload();
            })
            .fail(function(errors) {
                var msg = '';
                if(errors && errors.responseText) { //ajax error, errors = xhr object
                    msg = errors.responseText;
                }
                $('#msg').html(msg).removeClass('hide');

                $("#overlay").fadeOut();
            });
        });
    }


    // инитим редактирование товара
    function initUpdateOrderProduct(orderId) {
        $('.update-order-product').editable();

        $('.to-shipped-order').click(function() {
            $("#overlay").fakeLoader();
            if (!confirm("Точно заказ готов к отправке?")) {
                $("#overlay").fadeOut();
                return;
            }

            $.ajax({
                method: "PUT",
                url: "/order",
                data: { pk: orderId, name: 'status', value: 'shipped'}
            })
                .done(function( msg ) {
                    $("#overlay").fadeOut();

                    $('.order-products-modal').modal('hide');
                    table.ajax.reload();
                })
                .fail(function(msg){
                    $("#overlay").fadeOut();

                    alert('Ошибка');
                });
        });

        $('.close-order').click(function() {
            $("#overlay").fakeLoader();
            if (!confirm("Точно заказ готов?")) {
                $("#overlay").fadeOut();
                return;
            }

            $.ajax({
                method: "PUT",
                url: "/order",
                data: { pk: orderId, name: 'status', value: 'completed'}
            })
                .done(function( msg ) {
                    $("#overlay").fadeOut();

                    $('.order-products-modal').modal('hide');
                    table.ajax.reload();
                })
                .fail(function(msg){
                    $("#overlay").fadeOut();

                    alert('Ошибка');
                });
        });
    }

    if ($('#store-product-price').length) {

        initEditableProductFields();
        initProductsFields();

        // при отправке формы собираем введенные данные
        $('#save-btn').click(function() {
            $("#overlay").fakeLoader();

            var products = [];
            // пробигаемся по всем строкам добавленных продуктов, и собираем что ввели
            $('#store-product-price .product-item').each(function() {
                var val = $(this).find('a').editable('getValue');

                products.push(val);
            });

            if (products.length == 0) {
                alert('Нужно выбрать товары');

                $("#overlay").fadeOut();
                return;
            }

            $.ajax({
                method: "POST",
                url: location.href,
                data: {
                    products: products
                },
                dataType: 'json'
            })
            .done(function( msg ) {
                location.reload();
            })
            .fail(function(errors) {
                var msg = '';
                if(errors && errors.responseText) { //ajax error, errors = xhr object
                    msg = errors.responseText;
                }
                $('#msg').html(msg).removeClass('hide');


                $("#overlay").fadeOut();
            });
        });

    }


    // добавляем динамически строку продукта в таблицу
    function initProductsFields() {
        $('.add-product').click(function(e){
            e.preventDefault();
            var row = $('<tr class="product-item">' +
            '<td><a href="#" class="select2-product" data-type="select2" data-name="product_id" data-original-title="Товар">не указано</a></td>' +
            '<td><a href="#" data-type="text" data-name="price" data-original-title="Цена">0</a></td>' +
            '<td><button type="button" class="btn btn-default btn-xs remove-field"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> удалить</button></td>' +
            '</tr>');

            $('#store-product-price tbody').append(row);

            row.find('a:not(.select2-product)').editable();

            row.find('.select2-product').editable({
                source: productsList, // глобальный объект с продуктами
                select2: {
                    width: 200,
                    placeholder: 'Товар',
                    allowClear: false
                }
            });
        });

        //удалить строку с продуктом
        $('#store-product-price').on("click",".product-item .remove-field", function(e){
            e.preventDefault(); $(this).parents('.product-item').remove();
        });
    }

    function initEditableProductFields() {
        $('.product-item').each(function() {

            $(this).find('a:not(.select2-product)').editable();

            $(this).find('.select2-product').editable({
                source: productsList, // глобальный объект с продуктами
                select2: {
                    width: 200,
                    placeholder: 'Товар',
                    allowClear: false
                }
            });
        });
    }



    // отображается таблица товаров на складе магазина
    $('#store-stock-product-list-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter" : false,
        "bSort": false,
        "language" : $.datatableLang,
        "ajax": {
            "url": location.href,
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "count" }
        ],
        "columnDefs": [{
            "render": function ( data, type, row ) {
                return '<a target="_blank" href="/product/'+data+'">'+data+'</a>';
            },
            "targets": 0 // преобразуем ID товара в ссылку
        }]
    });

    // отображается таблица растрат магазина
    $('#store-charges-list-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter" : false,
        "bSort": false,
        "language" : $.datatableLang,
        "ajax": {
            "url": location.href,
            "type": "POST"
        },
        "columns": [
            { "data": "type" },
            { "data": "description" },
            { "data": "cost" },
            { "data": "date" }
        ]
    } );


    // создание новой растраты магазина
    if ($('#new-charge-store a').length) {

        // делаем так потому что автоматически селект2 не цепляет данные, нужно инитить отдельно
        $('#new-charge-store a:not(#charge)').editable();

        $('#charge').editable({
            source: $(this).data().source,
            select2: {
                width: 200,
                placeholder: $(this).data().originalTitle,
                allowClear: true
            }
        });

        // при отправке формы собираем введенные данные
        $('#save-btn').click(function() {
            $("#overlay").fakeLoader();

            $.ajax({
                method: "POST",
                url: location.href + '?new_charge=1',
                data: $('#new-charge-store a').editable('getValue'),
                dataType: 'json'
            })
                .done(function( msg ) {
                    location.reload();
                })
                .fail(function(errors) {
                    var msg = '';
                    if(errors && errors.responseText) { //ajax error, errors = xhr object
                        msg = errors.responseText;
                    }
                    $('#msg').html(msg).removeClass('hide');

                    $("#overlay").fadeOut();
                });
        });
    }
});