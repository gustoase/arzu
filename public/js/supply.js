$(function () {
    // отображается таблица поставок
    var table = $('#supply-list-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "bSort": false,
        "language" : $.datatableLang,
        "ajax": {
            "url": location.href,
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "provider" },
            { "data": "stock_name" },
            { "data": "date" },
            { "data": "products" }
        ],
        "columnDefs": [{
                "render": function ( data, type, row ) {
                    return '<a target="_blank" href="/stock/'+row.stock_id+'/products">'+data+'</a>';
                },
                "targets": 2 // преобразуем название склада в ссылку на склад
            }, {
                "render": function ( data, type, row ) {
                    var result = '<table class="products-table table table-bordered hidden">';
                        $.each(data, function(i, row) {
                            result += '<tr>';
                                result += '<td>' + row.name + '</td>';
                                result += '<td>' + row.count + ' шт.</td>';
                                result += '<td>' + row.price_sum + ' ('+row.price+') руб.' + '</td>';
                            result += '</tr>';
                        });

                    result += '</table>';
                    return result;
                },
                "targets": 4 // Выводим список товаров в поставке
            }]
    } );

    // при щелчке на строку с поставкой, отобразить поставку
    table.on( 'draw', function () {
        $('#supply-list-table tr').on('click', function() {
            var products = $(this).find('.products-table');

            if (products.hasClass('hidden')) {
                products.removeClass('hidden');
            } else {
                products.addClass('hidden');
            }
        });
    } );

    // создание новой поставки
    if ($('#new-supply a').length) {

        // делаем так потому что автоматически селект2 не цепляет данные, нужно инитить отдельно
        $('#new-supply a:not(#stock-id)').editable();

        $('#stock-id').editable({
            source: $(this).data().source,
            select2: {
                width: 200,
                placeholder: 'На какой склад?',
                allowClear: true
            }
        });

        // при отправке формы собираем введенные данные
        $('#save-btn').click(function() {
            $("#overlay").fakeLoader();

            var form = {
                provider : $('#provider').editable('getValue').provider,
                stock_id : $('#stock-id').editable('getValue').stock_id,
                products : []
            };

            // пробигаемся по всем строкам добавленных продуктов, и собираем что ввели
            $('#product-list tr').each(function() {
                var val = $(this).find('a').editable('getValue');

                form.products.push(val);
            });

            $.ajax({
                method: "POST",
                url: "/supply/new",
                data: form,
                dataType: 'json'
            })
            .done(function( msg ) {
                location.reload();
            })
            .fail(function(errors) {
                var msg = '';
                if(errors && errors.responseText) { //ajax error, errors = xhr object
                    msg = errors.responseText;
                }
                $('#msg').html(msg).removeClass('hide');

                $("#overlay").fadeOut();
            });
        });

        initProductsFields();
    }


    // добавляем динамически строку продукта в таблицу
    function initProductsFields() {
        $('.add-product').click(function(e){
            e.preventDefault();
            var row = $('<tr class="product-item">' +
                            '<td><a href="#" class="select2-product" data-type="select2" data-name="product_id" data-original-title="Товар">не указано</a></td>' +
                            '<td><a href="#" data-type="text" data-name="price" data-original-title="Цена">0.0</a></td>' +
                            '<td><a href="#" data-type="text" data-name="count" data-original-title="Кол-во">0</a></td>' +
                            '<td><button type="button" class="btn btn-default btn-xs remove-field"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> удалить</button></td>' +
                        '</tr>');

            $('#product-list tbody').append(row);

            row.find('a:not(.select2-product)').editable();

            row.find('.select2-product').editable({
                source: productsList, // глобальный объект с продуктами
                select2: {
                    width: 200,
                    placeholder: 'Товар',
                    allowClear: false
                }
            });
        });

        //удалить строку с продуктом
        $('#product-list').on("click",".product-item .remove-field", function(e){
            e.preventDefault(); $(this).parents('.product-item').remove();
        });
    }
});