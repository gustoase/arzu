$(function() {
    $('.date-filter').editable();


    $('#submit-date').click(function() {
        var date = $('.date-filter').editable('getValue');

        location.replace('/statistic?date_start='+date.date_start + '&date_end='+date.date_end);
    });

    $('.statistic-block-info').click(function() {
        var charts = $(this).next();

        if (charts.is(':visible')) {
            charts.hide();
        } else {
            charts.show();
        }
    });
});


function drawCharts() {
    $('.chart').each(function () {
        var el = this;
        var values = $(el).data().values;
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'X');
        data.addColumn('number', ' Выручка');

        // example data = [ [0, 0],   [1, 10],  [2, 23] ]
        var preparedData = [];

        if (typeof values != 'undefined') {
            $.each(values, function(date, value) {
                preparedData.push([
                    new Date(date), value
                ])
            });
        }

        data.addRows(preparedData);

        var options = {
            hAxis: {
                format: 'dd-MM-yyyy',
                title: 'В какой день'
            },
            vAxis: {
                title: 'Выручка'
            }
        };

        var chart = new google.visualization.LineChart(el);

        chart.draw(data, options);
    });


    $('.statistic-block-chart').hide();
}

google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawCharts);