$(function () {
    $('.radius-block').each(function() {
        var value = $(this).data().chartValue;
        var prefix = $(this).data().chartPrefix;

        radialProgress(this, prefix)
            .diameter(150)
            .value(value)
            .render();
    });

    // схлопывание панели
    $('.panel-toggle').click(function() {
        var body = $(this).next('.panel-body');
        if (body.hasClass('hidden')) {
            body.removeClass('hidden');
        } else {
            body.addClass('hidden');
        }
    });

    $.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.ajaxOptions = {
        type: 'put'
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.datatableLang = {
        "processing": "Подождите...",
        "search": "Поиск:",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют.",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
            "first": "Первая",
            "previous": "Предыдущая",
            "next": "Следующая",
            "last": "Последняя"
        },
        "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
        }
    };

    initTooltip();
    initDropDown();
});


function initTooltip() {
    $('[data-toggle="tooltip"]').tooltip();
}


function initDropDown() {
    $('[data-toggle="dropdown"]').dropdown();
}
