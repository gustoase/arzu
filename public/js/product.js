$(function () {

    // возможность редактировать товар
    if ($('#product-info a').length) {
        $('#product-info a').editable({
            url: $('#product-info').data().url
        });
    }

    // отображается таблица продуктов
    $('#product-list-table').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter" : false,
        "bSort": false,
        "language" : $.datatableLang,
        "ajax": {
            "url": location.href,
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "description" },
            { "data": "in_box" },
            { "data": "price" },
            { "data": "price_box" }
        ],
        "columnDefs": [{
                "render": function ( data, type, row ) {
                    return '<a href="/product/'+data+'">'+data+'</a>';
                },
                "targets": 0 // преобразуем ID товара в ссылку
            }]
    } );

    // удалить продукт
    $('#remove-product').click(function() {
        if (!confirm("Вы подтверждаете удаление?")) {
            return;
        }

        $("#overlay").fakeLoader();

        var productId = $(this).data().id;

        $.ajax({
            method: "DELETE",
            url: "/product/" + productId
        })
        .done(function( msg ) {
            window.history.back();
        });
    });

    // создание нового товара
    if ($('#new-product a').length) {

        var newProduct = $('#new-product a');
        newProduct.editable();

        $('#save-btn').click(function() {
            $("#overlay").fakeLoader();

            newProduct.editable('submit', {
                type: 'post',
                url: '/product/new',
                ajaxOptions: {
                    dataType: 'json' //assuming json response
                },
                success: function(data) {
                    location.reload();
                },
                error: function(errors) {
                    var msg = '';
                    if(errors && errors.responseText) { //ajax error, errors = xhr object
                        msg = errors.responseText;
                    }
                    $('#msg').html(msg).removeClass('hide');
                    $("#overlay").fadeOut();
                }
            });
        });
    }
});