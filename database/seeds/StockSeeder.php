<?php

use App\Stock;
use Illuminate\Database\Seeder;

/**
 * Class StockSeeder
 * @package database\seeds
 */
class StockSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
                'name' => 'Садовод',
                'address' => 'МКАД 44км',
                'phone' => '8923847124',
                'description' => 'Описание садовода'
            ],
            [
                'name' => 'Савеловская',
                'address' => 'Москва ул. медведя',
                'phone' => '1234141414',
                'description' => 'Описание Савеловская'
            ],
            [
                'name' => 'Подольск',
                'address' => 'Москва ул. маршала',
                'phone' => '4442424244',
                'description' => 'Описание Подольск'
            ]
        ];

        foreach($data as $row) {
            Stock::create($row);
        }
    }
}