<?php


use App\User;
use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role;
use Illuminate\Support\Facades\Hash;

/**
 * Первоначальные юзера и права
 * Class UserSeeder
 */
class UserSeeder extends Seeder {

    public function run()
    {
        $admin = User::create( [
            'email' => 'bagzon@gmail.com',
            'password' => Hash::make('bagzon'),
            'name' => 'Alexandr D.',
            'phone' => '89164423295',
        ] );

        $adminRole = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Полный доступ', // optional
            'level' => 1, // optional, set to 1 by default
        ]);

        $admin->attachRole($adminRole);



        $moderator = User::create( [
            'email' => 'moderator@gmail.com',
            'password' => Hash::make('moderator'),
            'name' => 'Moderator name',
            'phone' => '123456789',
        ]);

        $moderatorRole = Role::create([
            'name' => 'moderator',
            'slug' => 'moderator',
            'description' => 'У кого чуть меньше прав чем у админа',
            'level' => 2,
        ]);

        $moderator->attachRole($moderatorRole);



        $store = User::create( [
            'email' => 'store@gmail.com',
            'password' => Hash::make('store'),
            'name' => 'Store name',
            'phone' => '987654321',
        ]);

        $storeRole = Role::create([
            'name' => 'Store',
            'slug' => 'store',
            'description' => 'Управляющий магазином, может создавать заказы и управлять магазином',
            'level' => 3,
        ]);

        $store->attachRole($storeRole);
    }
}