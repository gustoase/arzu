<?php

use App\Stock;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductSeeder
 * @package database\seeds
 */
class ProductSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        DB::table('product')->delete();
        DB::table('supply')->delete();
        DB::table('supply_product')->delete();
        DB::table('stock_product')->delete();

        $data = [
            [
                'name' => 'Пакет черный',
                'description' => 'Описание черного пакета',
                'in_box' => 100,
                'price' => 0.30,
            ],
            [
                'name' => 'Пакет белый',
                'description' => 'Описание белого пакета',
                'in_box' => 80,
                'price' => 0.10,
            ],
            [
                'name' => 'Скотч',
                'description' => 'Описание скотча',
                'in_box' => 300,
                'price' => 2.4,
            ],
            [
                'name' => 'Ножницы',
                'description' => 'Описание ножницы',
                'in_box' => 30,
                'price' => 10.2,
            ],
            [
                'name' => 'Мусорный пакет',
                'description' => 'Описание пакета ля мусора',
                'in_box' => 300,
                'price' => 0.13,
            ],
        ];


        $products = [];
        foreach($data as $row) {
            $product = \App\Product::create($row);

            $products[] = $product;
        }

        foreach(Stock::all() as $stock) {

            // пришла поставка от поставщика
            $supply = \App\Supply::create([
                'stock_id' => $stock->id,
                'provider' => 'provider_' . rand(1, 99)
            ]);

            // поставка состоит из таких вот товаров
            foreach($products as $product) {
                $count = rand(20, 80);

                \App\SupplyProduct::create([
                    'supply_id' => $supply->id,
                    'product_id' => $product->id,
                    'count' => $count,
                    'price' => $product->price
                ]);

                \App\StockProduct::create([
                    'stock_id'   => $stock->id,
                    'product_id' => $product->id,
                    'price' => $product->price,
                    'count' => $count,
                    'last_supply_id' => $supply->id
                ]);
            }
        }
    }
}