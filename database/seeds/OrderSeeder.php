<?php

use App\Order;
use App\OrderProduct;
use App\Store;
use App\StoreProduct;
use Illuminate\Database\Seeder;

/**
 * Class OrderSeeder
 * @package database\seeds
 */
class OrderSeeder extends Seeder
{

    public function run()
    {

        // создаем магазин пользователю
        $store = new Store();
        $store->user_id = 3;
        $store->name = 'Мой магазин';
        $store->description = 'Описание магазина';
        $store->save();

        // далее магазин делает заказ
        $order = new Order();
        $order->store_id = $store->id;
        $order->status = Order::STATUS_NEW;
        $order->description = 'описание заказа';
        $order->save();


        // что в заказе
        $data = [
            [
                'product_id' => 21,
                'count'      => 15,
                'price_in'   => 0.7
            ],
            [
                'product_id' => 22,
                'count'      => 8,
                'price_in'   => 1.2
            ],
            [
                'product_id' => 23,
                'count'      => 40,
                'price_in'   => 0.3
            ],
        ];

        $balance = 0;
        foreach ($data as $item) {
            $orderProduct = new OrderProduct();
            $orderProduct->order_id = $order->id;
            $orderProduct->product_id = $item['product_id'];
            $orderProduct->count = $item['count'];
            $orderProduct->price_in = $item['price_in'];

            $storeProduct = new StoreProduct();
            $storeProduct->store_id = $store->id;
            $storeProduct->product_id = $item['product_id'];
            $storeProduct->count = $item['count'];

            $balance += $item['count'] * $item['price_in'];
            $orderProduct->save();
            $storeProduct->save();
        }

        $store->balance = -$balance;
        $store->save();
    }
}