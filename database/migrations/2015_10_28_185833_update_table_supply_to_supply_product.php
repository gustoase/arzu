<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableSupplyToSupplyProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // переименуем таблицу поставок в таблицу продуктов в поставке
        Schema::table('supply', function (Blueprint $table) {
            $table->string('supply_id');
            $table->dropColumn('provider');
            $table->dropColumn('stock_id');
        });

        Schema::rename('supply', 'supply_product');

        // создадим таблицу поставок
        Schema::create('supply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_id');
            $table->string('provider'); // кто поставщик
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supply_product');
    }
}
