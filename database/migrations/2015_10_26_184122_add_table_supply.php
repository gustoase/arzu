<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSupply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // поставки товаров на склад
        Schema::create('supply', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_id');
            $table->integer('product_id');
            $table->integer('count');
            $table->float('price'); // цена за единицу
            $table->string('provider'); // кто поставщик
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supply');
    }
}
