<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableStockProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // какие товары есть на складе и кол-во
        Schema::create('stock_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stock_id');
            $table->integer('product_id');
            $table->float('price');
            $table->integer('count');
            $table->integer('last_supply_id'); // id последней поставки на склад
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_product');
    }
}
